<!DOCTYPE html>
<html class="backend">
    <!-- START Head -->
    <head>
        <!-- START META SECTION -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>LOGIN</title>
        <meta name="author" content="pampersdry.info">
        <meta name="description" content="Adminre is a clean and flat backend and frontend theme build with twitter bootstrap">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">

        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="{{asset('temp/image/touch/apple-touch-icon-144x144-precomposed.png')}}">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="{{asset('temp/image/touch/apple-touch-icon-114x114-precomposed.png')}}">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="{{asset('temp/image/touch/apple-touch-icon-72x72-precomposed.png')}}">
        <link rel="apple-touch-icon-precomposed" href="{{asset('temp/image/touch/apple-touch-icon-57x57-precomposed.png')}}">
        <link rel="shortcut icon" href="{{asset('temp/image/favicon.ico')}}">
        <!--/ END META SECTION -->

        <!-- START STYLESHEETS -->
        <!-- Plugins stylesheet : optional -->
        <!--/ Plugins stylesheet : optional -->

        <!-- Application stylesheet : mandatory -->
        <link rel="stylesheet" href="{{asset('temp/stylesheet/bootstrap.css')}}">
        <link rel="stylesheet" href="{{asset('temp/stylesheet/layout.css')}}">
        <link rel="stylesheet" href="{{asset('temp/stylesheet/uielement.css')}}">
        <!--/ Application stylesheet -->

        <!-- Theme stylesheet : optional -->
        <!--/ Theme stylesheet : optional -->

        <!-- modernizr script -->
        <script type="text/javascript" src="{{asset('temp/plugins/modernizr/js/modernizr.js')}}"></script>
        <!--/ modernizr script -->
        <!-- END STYLESHEETS -->
    </head>
    <!--/ END Head -->

    <!-- START Body -->
    <body>
        <!-- START Template Main -->
        <section id="main" role="main">
            <!-- START Template Container -->
            <section class="container">
                <!-- START row -->
                <br>
                <div class="row">
                    <div class="col-lg-4 col-lg-offset-4">
                        <!-- Brand -->
                        <div class="text-center" style="margin-bottom:40px;">
                            <!-- <span class="logo-figure inverse">SyGeC-ONG-BLOOM</span> -->
                            <!-- <span class="bold mt-5"></span> -->
                            <!-- <h5 class="semibold text-muted mt-5">SyGeC-ONG-BLOOM</h5> -->
                            <div class="login-logo mt-5">
                                <a class="mt-25" style="font-size: 24px;" href="{{ route('accueil') }}"><b>ADMIN</b> </a>
                            </div>
                        </div>
                        <!--/ Brand -->




                        <hr><!-- horizontal line -->

                        <!-- Login form -->
                        <form class="panel" name="form-login" method="POST" action="{{ route('login') }}">
                            @if (Session::has('flash_message'))
                            <div class="container">
                                <div class="alert alert-success">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    {{ Session::get('flash_message') }}
                                </div>
                            </div>
                        @endif
                        <div class="container">
                            @include('flash::message')
                        </div>
                            @csrf
                            <div class="panel-body">
                                <!-- Alert message -->
                                <div class="alert alert-warning">
                                    <span class="semibold">Note :</span>&nbsp;&nbsp;Espace réservé uniquement aux personnes <b>Autorisées</b>.
                                </div>
                                <!--/ Alert message -->

                                <div class="form-group">
                                    <div class="form-stack has-icon pull-left">
                                        <input type="email" class="form-control input-lg @error('email') is-invalid @enderror" placeholder="email" data-parsley-errors-container="#error-container" data-parsley-error-message="Entrez une adresse email valide" data-parsley-required name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                                        <i class="ico-user2 form-control-icon"></i>
                                        @error('email')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                    <div class="form-stack has-icon pull-left">
                                        <input id="password" name="password" type="password" class="form-control input-lg @error('password') is-invalid @enderror" placeholder="Mot de Passe" data-parsley-errors-container="#error-container" data-parsley-error-message="Entrez le bon Mot de Passe" data-parsley-required required autocomplete="current-password">
                                        <i class="ico-lock2 form-control-icon"></i>
                                        @error('password')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>

                                <!-- Error container -->
                                <div id="error-container"class="mb15"></div>
                                <!--/ Error container -->

                                <div class="form-group">
                                    <div class="row">

                                        <div class="col-xs-12 text-right">
                                            <a href="{{ route('password.request') }}">Mot de Passe Oublé?</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group nm">
                                    <button type="submit" class="btn btn-success"><span class="semibold">Connexion</span></button>
                                    <button type="reset" class="btn btn-danger"><span class="semibold">Annuler</span></button>
                                </div>
                            </div>
                        </form>
                        <!-- Login form javascript:void(0); -->

                        <hr><!-- horizontal line -->

                        <p class="text-muted text-center">© Copyright 2021 <a target="_blanck" class="semibold" href="http://www.enseignement.gouv.ci/">ONG-BLOOM</a> Tous droits réservés.</p>
                    </div>
                </div>
                <!--/ END row -->
            </section>
            <!--/ END Template Container -->
        </section>
        <!--/ END Template Main -->

        <!-- START JAVASCRIPT SECTION (Load javascripts at bottom to reduce load time) -->
        <!-- Application and vendor script : mandatory -->
        <script type="text/javascript" src="{{asset('temp/javascript/vendor.js')}}"></script>
        <script type="text/javascript" src="{{asset('temp/javascript/core.js')}}"></script>
        <script type="text/javascript" src="{{asset('temp/javascript/backend/app.js')}}"></script>
        <!--/ Application and vendor script : mandatory -->

        <!-- Plugins and page level script : optional -->
        <script type="text/javascript" src="{{asset('temp/plugins/parsley/js/parsley.js')}}"></script>
        <!-- <script type="text/javascript" src="{{asset('temp/javascript/backend/pages/login.js')}}"></script> -->
        <!--/ Plugins and page level script : optional -->
        <!--/ END JAVASCRIPT SECTION -->
    </body>
    <!--/ END Body -->
</html>
