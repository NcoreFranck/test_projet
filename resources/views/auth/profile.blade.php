@extends('layouts.layout')

@section('content')
    <!-- START Template Container -->
    <div class="container-fluid">
        <!-- Page Header -->
        <div class="page-header page-header-block">
            <div class="page-header-section">
                <h4 class="title semibold">Modifier Profile</h4>
            </div>
            <div class="page-header-section">
                <!-- Toolbar -->
                <div class="toolbar clearfix">
                    <div class="col-xs-8">
                        
                    </div>
                    <!-- <div class="col-xs-4">
                        <button class="btn btn-primary pull-right"><i class="ico-plus mr5"></i>Créer</button>
                    </div> -->
                </div>
                <!--/ Toolbar -->
            </div>
        </div>
        <!-- Page Header -->
        <!-- START row -->
        <div class="row">
            <div class="col-md-12">
                <!-- Form default layout -->
                <form class="panel panel-default" method="POST" action="{{route('update_profile')}}">
                    <div class="panel-heading">
                        <h3 class="panel-title">Modifier Profile</h3>
                    </div>
                    {!! csrf_field() !!}
                    <div class="panel-body">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-sm-6">
                                    <label class="control-label">Nom & Prénoms</label>
                                    <input name="name" type="text" required="" class="form-control" placeholder="NOM Prenoms" value="{{Auth::user()->name}}">
                                </div>
                                <div class="col-sm-6">
                                    <label class="control-label">Email</label>
                                    <input name="email" placeholder="email@email.com" type="email" required="" class="form-control" value="{{Auth::user()->email}}">
                                </div>
                            </div>
                        </div>
                        <div class="form-group" id="check">
                            <div class="checkbox custom-checkbox">
                                <input type="checkbox" name="gift" id="giftcheckbox" value="1">
                                <label for="giftcheckbox">Editer le Mot de Passe</label>
                            </div>
                        </div>
                        <div class="form-group password_cache" id="password">
                            <div class="row">
                                <div class="col-sm-4 mb10">
                                    <label class="control-label">Mot de Passe Actuel</label>
                                    <input name="password_actuel" type="password" class="form-control" placeholder="Mot de Passe Actuel">
                                </div>
                                <div class="col-sm-4 mb10">
                                    <label class="control-label">Nouveau Mot de Passe</label>
                                    <input name="password" type="password" class="form-control" placeholder="Mot de Passe">
                                </div>
                                <div class="col-sm-4 mb10">
                                    <label class="control-label">Confirmer Mot de Passe</label>
                                    <input name="password_confirm" type="password" class="form-control" placeholder="Confirmer Mot de Passe">
                                </div>
                            </div>
                        </div>

                    </div>
                <div class="panel-footer">
                    <button type="submit" class="btn btn-primary">Enregistrer</button>
                    <a href="javascript:history.back()" class="btn btn-inverse">Annuler</a>
                </div>
            </form>
                    <!--/ Form default layout -->
        </div>
    </div>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script>
$(document).ready(function () {
$('#password').hide();
//lorsqu'on coche la case
  $('#giftcheckbox').click(function() {
    $('#password').toggle(800);
  });
});
 </script>
@endsection
