<!DOCTYPE html>
<html class="backend">
    <!-- START Head -->
    <head>
        <!-- START META SECTION -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Système de Gestion de Courrier - MESRS</title>
        <meta name="author" content="pampersdry.info">
        <meta name="description" content="Adminre is a clean and flat backend and frontend theme build with twitter bootstrap">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">

        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="{{asset('temp/image/touch/apple-touch-icon-144x144-precomposed.png')}}">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="{{asset('temp/image/touch/apple-touch-icon-114x114-precomposed.png')}}">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="{{asset('temp/image/touch/apple-touch-icon-72x72-precomposed.png')}}">
        <link rel="apple-touch-icon-precomposed" href="{{asset('temp/image/touch/apple-touch-icon-57x57-precomposed.png')}}">
        <link rel="shortcut icon" href="{{asset('temp/image/favicon.ico')}}">
        <!--/ END META SECTION -->

        <!-- START STYLESHEETS -->
        <!-- Plugins stylesheet : optional -->
        <!--/ Plugins stylesheet : optional -->

        <!-- Application stylesheet : mandatory -->
        <link rel="stylesheet" href="{{asset('temp/stylesheet/bootstrap.css')}}">
        <link rel="stylesheet" href="{{asset('temp/stylesheet/layout.css')}}">
        <link rel="stylesheet" href="{{asset('temp/stylesheet/uielement.css')}}">
        <!--/ Application stylesheet -->

        <!-- Theme stylesheet : optional -->
        <!--/ Theme stylesheet : optional -->

        <!-- modernizr script -->
        <script type="text/javascript" src="{{asset('temp/plugins/modernizr/js/modernizr.js')}}"></script>
        <!--/ modernizr script -->
        <!-- END STYLESHEETS -->
    </head>
    <!--/ END Head -->

    <!-- START Body -->
    <body>
        <!-- START Template Main -->
        <section id="main" role="main">
            <!-- START Template Container -->
            <section class="container">
                <!-- START row -->
                <br>
                <div class="row">
                    <div class="col-lg-4 col-lg-offset-4">
                        <!-- Brand -->
                        <div class="text-center" style="margin-bottom:40px;">
                            <!-- <span class="logo-figure inverse">SyGeC-MESRSCI</span> -->
                            <!-- <span class="bold mt-5"></span> -->
                            <!-- <h5 class="semibold text-muted mt-5">SyGeC-MESRSCI</h5> -->
                            <div class="login-logo mt-5">
                                <a class="mt-25" style="font-size: 24px;" href="{{ route('index') }}"><b>SyGeC</b>-MESRSCI</a>
                            </div>
                        </div>
                        <!--/ Brand -->


                        

                        <hr><!-- horizontal line -->

                        <!-- Login form -->
                        <form class="panel" name="form-login" method="POST" action="{{ route('password.email') }}">
                            @csrf
                            <div class="panel-body">
                                <!-- Alert message -->
                                <div class="alert alert-warning">
                                    <span class="semibold">Réinitialisation du Mot de Passe</span>
                                </div>
                                <!--/ Alert message -->
                                
                                <div class="form-group">
                                    <div class="form-stack has-icon pull-left">
                                        <input id="email" type="email" class="form-control input-lg @error('email') is-invalid @enderror" placeholder="email" data-parsley-errors-container="#error-container" data-parsley-error-message="Entrez une adresse email valide" data-parsley-required name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                                        <i class="ico-user2 form-control-icon"></i>
                                        @error('email')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>

                                <!-- Error container -->
                                <div id="error-container"class="mb15"></div>
                                <!--/ Error container -->
                                <div class="form-group nm">
                                    <button type="submit" class="btn btn-block btn-success"><span class="semibold">Envoyer</span></button>
                                </div>
                            </div>
                        </form>
                        <!-- Login form javascript:void(0); -->

                        <hr><!-- horizontal line -->

                        <p class="text-muted text-center">© Copyright 2020 <a target="_blanck" class="semibold" href="http://www.enseignement.gouv.ci/">MESRSCI</a> Tous droits réservés.</p>
                    </div>
                </div>
                <!--/ END row -->
            </section>
            <!--/ END Template Container -->
        </section>
        <!--/ END Template Main -->

        <!-- START JAVASCRIPT SECTION (Load javascripts at bottom to reduce load time) -->
        <!-- Application and vendor script : mandatory -->
        <script type="text/javascript" src="{{asset('temp/javascript/vendor.js')}}"></script>
        <script type="text/javascript" src="{{asset('temp/javascript/core.js')}}"></script>
        <script type="text/javascript" src="{{asset('temp/javascript/backend/app.js')}}"></script>
        <!--/ Application and vendor script : mandatory -->

        <!-- Plugins and page level script : optional -->
        <script type="text/javascript" src="{{asset('temp/plugins/parsley/js/parsley.js')}}"></script>
        <script type="text/javascript" src="{{asset('temp/javascript/backend/pages/login.js')}}"></script>
        <!--/ Plugins and page level script : optional -->
        <!--/ END JAVASCRIPT SECTION -->
    </body>
    <!--/ END Body -->
</html>
