@extends('layouts.layout_front')
@section('content')

    <style>
        #hero .carousel-content {
            width: 100%;
        }
        #hero {
            height: 50vh;
        }
    </style>

    <div style="padding-bottom: 30px;" class="container">
        <div class="row">
            <div class="col-lg-7">
                <section id="hero">
                    <div id="heroCarousel" class="carousel slide carousel-fade" data-ride="carousel">

                        <div class="carousel-inner" role="listbox">

                            <!-- Slide 1 -->
                            @foreach($bannieres as $banniere)
                                <div class="carousel-item {{$banniere->etat}}"
                                     style="background-image: url({{asset($banniere->source)}});">
                                    <!--
                                    <div class="carousel-container">
                                        <div class="carousel-content animate__animated animate__fadeInUp">
                                            <h2>{{$banniere->titre}}</h2>
                                            <p>{{$banniere->description}}</p>
                                            <div class="text-center">
                                                <a href="{{route('front_mediatheque')}}" class="btn btn-warning">MES ACTIONS</a>
                                                <a href="" class="btn-get-started">Devient un membre</a>
                                            </div>
                                        </div>
                                    </div>
                                    -->
                                </div>
                            @endforeach

                        </div>

                        <a class="carousel-control-prev" href="#heroCarousel" role="button" data-slide="prev">
                            <span class="carousel-control-prev-icon bx bx-left-arrow" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>

                        <a class="carousel-control-next" href="#heroCarousel" role="button" data-slide="next">
                            <span class="carousel-control-next-icon bx bx-right-arrow" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>

                        <ol class="carousel-indicators" id="hero-carousel-indicators"></ol>

                    </div>
                </section>
            </div>
            <div class="col-lg-5">
                <section id="hero">
                    <div class="carousel-content animate__animated animate__fadeInUp">
                        <h2>{{$banniere_active->titre}}</h2>
                        <p>{{$banniere_active->description}}</p>
                        <div class="text-center">
                            <a href="{{route('front_mediatheque')}}" class="btn btn-warning">MES ACTIONS</a>
                            <a href="#devenir_membre" class="btn-get-started">Devenir un membre</a>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>


    <main id="main">
        <!-- ======= Cta Section ======= -->
        <section id="cta" class="cta">
            <div class="container">

                <div style=" background: #fff; padding: 15px" class="row">
                    <div class="container">
                        @include('flash::message')
                    </div>
                    <div style="line-height: 36px; background: #fff" class="col-lg-6 text-center text-lg-left">
                        <!--
                        <h3>Qui est Madame <span>.....</span></h3>
                        <p></p>
                        -->
                        {!! $banniere_active->autre_info !!}
                    </div>
                    <div id="devenir_membre" class="col-lg-6">
                        @include('__partials.form_devient_membre')
                    </div>
                </div>

            </div>
        </section><!-- End Cta Section -->


    </main>
@endsection()
