@extends('layouts.layout_front')
@section('content')

<main id="main">
  <section id="portfolio" class="portfolio">
      <div class="container">

        <div class="section-title" data-aos="fade-up">
        </div>

        <div class="row portfolio-container" data-aos="fade-up">
          @foreach($mediatheques as $mediatheque)
          <div class="col-lg-4 col-md-6 portfolio-item filter-card">
            <a href="{{route('mediatheque_view', $mediatheque->id)}}">
              <img src="{{asset($mediatheque->source)}}" style="width:500px; height:230px" class="img-fluid" alt="">
            </a>
            <div class="portfolio-info">
              <h6 style="font-size:12px; padding-bottom:22px">{{$mediatheque->libelle}}</h6>
              <p style="font-size:10px">Date de publication: {{\Carbon\Carbon::parse($mediatheque->created_at)->format('d/m/Y')}}</p>
              <a href="{{asset($mediatheque->source)}}" data-gall="portfolioGallery" class="venobox preview-link"><i class="bx bx-plus"></i></a>
              <a href="{{route('mediatheque_view', $mediatheque->id)}}" class="details-link" title="More Details"><i class="bx bx-link"></i></a>
            </div>
          </div>
          @endforeach

        </div>

      </div>
    </section>
</main>
@endsection()
