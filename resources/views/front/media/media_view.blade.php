@extends('layouts.layout_front')
@section('content')

    <style>
        .cta {
            background: #f3f1f0;
            padding: 15px;
            color: #574741;
        }
    </style>

    <main id="main">
        <section id="portfolio" class="portfolio">
            <div class="container">
                <br>
                <div class="section-title" data-aos="fade-up">
                    <hr>
                </div>
                <div class="container">
                    @include('flash::message')
                </div>
                <div class="row">
                    <div class="col-lg-6">
                        <section id="portfolio" class="portfolio">
                            <div class="row portfolio-container">
                                <div class="col-lg-12 portfolio-item filter-web">
                                    <img src="{{asset($mediatheque->source)}}" class="img-fluid" alt="">
                                    <div class="portfolio-info">
                                        <h4>{{$mediatheque->titre}}</h4>
                                        <p style="font-size:10px"><b></b></p>
                                        <a href="{{asset($mediatheque->source)}}" data-gall="portfolioGallery"
                                           class="venobox preview-link"><i class="bx bx-money"></i></a>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>
                    <div class="col-lg-6">
                        <div id="testimonials" class="testimonials">
                            <div class="col-lg-12">
                                <div class="testimonial-item">
                                    <h3><b>INFORMATIONS</b></h3>
                                    <p> {!!$mediatheque->libelle!!}</p>
                                    <p style="padding-top:20px"><b><i class="fa fa-calendar" aria-hidden="true"></i>
                                            DATE DE PUBLICATION:</b>
                                        <br> {{\Carbon\Carbon::parse($mediatheque->created_at)->format('d/m/Y')}}
                                        à {{\Carbon\Carbon::parse($mediatheque->created_at)->format('H:i:s')}}</p>
                                    @if(is_null($publication_rate))
                                        <div style="padding-top:30px">
                                            <i id="selector_star1" data-value="1" class="selector_star fa fa-star-o"
                                               aria-hidden="true"></i>
                                            <i id="selector_star2" data-value="2" class="selector_star fa fa-star-o"
                                               aria-hidden="true"></i>
                                            <i id="selector_star3" data-value="3" class="selector_star fa fa-star-o"
                                               aria-hidden="true"></i>
                                            <i id="selector_star4" data-value="4" class="selector_star fa fa-star-o"
                                               aria-hidden="true"></i>
                                            <i id="selector_star5" data-value="5" class="selector_star fa fa-star-o"
                                               aria-hidden="true"></i>
                                        </div>
                                    @else
                                        <div style="padding-top:30px">
                                            @for($i = 1; $i <= $publication_rate->note; $i++)
                                                <i class="selector_star_check fa fa-star-o"
                                                   aria-hidden="true"></i>
                                            @endfor
                                            @for($i = 1; $i <= 5-$publication_rate->note; $i++)
                                                <i class="selector_star fa fa-star-o"
                                                   aria-hidden="true"></i>
                                            @endfor
                                        </div>
                                    @endif
                                    <input type="hidden" name="_publication" value="{{$mediatheque->id}}">

                                    @if(is_null($membre))
                                        <div class="cta devient_membre" style="padding-top:20px; margin-top: 10px">
                                            @include('__partials.form_devient_membre')
                                        </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div>
                            <div class="col-lg-12">
                                <div class="testimonial-item">
                                    <span>{!!$mediatheque->description!!}</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <br><br>

            </div>
        </section>
    </main>
@endsection()

@section('js')
    <script type="text/javascript">
        $(document).ready(function () {
            $(".devient_membre").hide();
            $(".selector_star").on("mouseover", function () {
                var note = $(this).attr("data-value");
                if (note == 1) {
                    $("#selector_star1").addClass("selector_star_check");
                } else if (note == 2) {
                    $("#selector_star1").addClass("selector_star_check");
                    $("#selector_star2").addClass("selector_star_check");
                } else if (note == 3) {
                    $("#selector_star1").addClass("selector_star_check");
                    $("#selector_star2").addClass("selector_star_check");
                    $("#selector_star3").addClass("selector_star_check");
                } else if (note == 4) {
                    $("#selector_star1").addClass("selector_star_check");
                    $("#selector_star2").addClass("selector_star_check");
                    $("#selector_star3").addClass("selector_star_check");
                    $("#selector_star4").addClass("selector_star_check");
                } else if (note == 5) {
                    $("#selector_star1").addClass("selector_star_check");
                    $("#selector_star2").addClass("selector_star_check");
                    $("#selector_star3").addClass("selector_star_check");
                    $("#selector_star4").addClass("selector_star_check");
                    $("#selector_star5").addClass("selector_star_check");
                } else {

                }
                $(".devient_membre").show();
                $("[name='_publication_note']").val(note);
                save_publication_rate(note);
            });
        });

        function save_publication_rate(note) {
            var _note = note;
            var _publication = $("[name='_publication']").val();
            $.ajax({
                url: "{{route('save_publication_rate')}}",
                type: "POST",
                data: {_note: _note, _publication: _publication, "_token": "{{ csrf_token() }}",},
                success: function (response) {
                    if (response.success == "Successfully") {
                        $(".devient_membre").hide();
                    } else {

                    }
                },
                error: function (response) {
                },
            });
        }
    </script>
@endsection
