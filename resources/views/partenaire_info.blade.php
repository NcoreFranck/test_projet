@extends('layouts.layout_front')
@section('content')

<main id="main">

  <section id="clients" class="clients">
    <div class="container">


      <hr>
      <div class="row" data-aos="fade-up">
        <div class="col-lg-6">
          <a target="_blank" href="{{$partenaire->site_internet ?? '#'}}">
            <img src="{{asset($partenaire->logo)}}" class="img-fluid responsive" width="15%" alt="">
          </a>
        </div>
        <div align="right" class="col-lg-6">
          <a target="_blank" href="{{$partenaire->site_internet ?? '#'}}">
          <h3><b>{{$partenaire->entreprise_nom}}</b> </h3>
        </a>
        </div>
      </div>
      <hr>

      <section id="blog" class="blog">
      <div style="width:80%" class="container">

        <div class="row">

          <div class="col-lg-12 entries">

            <article class="entry" data-aos="fade-up">
              <h2 class="entry-title">
                <a href="{{$partenaire->site_internet}}">{{$partenaire->entreprise_nom}}</a>
              </h2>

              <div class="entry-content">
                <p>
                  {!!$partenaire->description!!}
                </p>
                @if($partenaire->site_internet != '')
                <div class="read-more">
                  <a target="_blank" href="{{$partenaire->site_internet ?? '#'}}">En savoir plus</a>
                </div>
                @endif
              </div>
            </article>

          </div>

          @if($partenaire->lien_video)
          <div align="center" class="col-lg-12">
            <div class="sidebar" data-aos="fade-left">
              <h3 class="sidebar-title">Media</h3>
              {!!$partenaire->lien_video!!}
              </div>
            </div>
            @endif
          </div>

        </div>

      </div>
    </section>

    @include('__partials.before_partenaires')

    </div>
  </section>

</main>
@endsection()
