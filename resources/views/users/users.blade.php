@extends('layouts.layout')

@section('content')
    <!-- START Template Container -->
    <div class="container-fluid">
        <!-- Page Header -->
        <div class="page-header page-header-block">
            <div class="page-header-section">
                <h4 class="title semibold">Liste des Utilisateurs</h4>
            </div>
            <div class="page-header-section">
                <!-- Toolbar -->
                <div class="toolbar clearfix">
                    <div class="col-xs-8">
                        <select class="form-control text-left" id="selectize-customselect">
                            <option value="0">Display metrics...</option>
                            <option value="1">Last 6 month</option>
                            <option value="2">Last 3 month</option>
                            <option value="3">Last month</option>
                        </select>
                    </div>
                    <div class="col-xs-4">
                        <a class="btn btn-primary pull-right" href="{{ route('add_user') }}"><i class="ico-user-plus2 mr5"></i>Ajouter</a>
                    </div>
                </div>
                <!--/ Toolbar -->
            </div>
        </div>
        <!-- Page Header -->
        <!-- START row -->
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default" id="demo">
                    <div class="panel-heading">
                        <h3 class="panel-title">Liste des Utilisateurs</h3>
                    </div>
                    <table class="table table-striped table-bordered" id="table-tools">
                        <thead>
                            <tr>
                                <th>N°</th>
                                <th>Nom</th>
                                <th>email</th>
                                <th>Rôles</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php $i=1; ?>
                        @forelse($users as $user)
                            <tr>
                                <td>{{$i++}}</td>
                                <td>{{$user->name}}</td>
                                <td>{{$user->email}}</td>
                                <td>
                                  @forelse ($user->roles()->pluck('name') as $role)
                                   <span class="label label-primary">{{ $role }}</span>
                               @empty
                                   <span class="label label-warning">Aucun Role</span>
                               @endforelse
                                </td>
                                <td>
                                    <!-- <a title="Voir" class="label label-success" href="">
                                        <i class="ico-eye-open">&nbsp;</i>
                                    </a> -->
                                    <a title="Modifier" class="label label-warning" href="{{ route('edit_user', $user->uuid)}}">
                                        <i class="ico-edit"></i>
                                    </a>
                                    <a title="Supprimer" class="label label-danger" href="javascript:void(0);">
                                        <form action="{{ route('delete_user') }}" method="post">
                                            @csrf
                                            <input type="hidden" name="user_uuid" value={{$user->uuid}} />
                                            <button onclick="return confirm('Voulez-vous vraiment Supprimer ?')" style="background-color: transparent; border: 0px;" type="submit" name="submit">
                                              <i style="color:#fff" class="ico-trash">
                                              </i>
                                            </button>
                                        </form>
                                    </a>
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <th colspan="5">Aucune données</th>
                            </tr>
                        @endforelse
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <!--/ END row -->
    </div>
@endsection
