@extends('layouts.layout')

@section('content')
    <!-- START Template Container -->
    <div class="container-fluid">
        <!-- Page Header -->
        <div class="page-header page-header-block">
            <div class="page-header-section">
                <h4 class="title semibold">Rôles</h4>
            </div>
            <div class="page-header-section">
                <!-- Toolbar -->
                <div class="toolbar clearfix">
                    <div class="col-xs-8">
                        <select class="form-control text-left" id="selectize-customselect">
                            <option value="0">Display metrics...</option>
                            <option value="1">Last 6 month</option>
                            <option value="2">Last 3 month</option>
                            <option value="3">Last month</option>
                        </select>
                    </div>
                    <div class="col-xs-4">
                        <a href="{{ route('create_role')}}"><button class="btn btn-primary pull-right"><i class="ico-plus mr5"></i>Créer</button></a>
                    </div>
                </div>
                <!--/ Toolbar -->
            </div>
        </div>
        <!-- Page Header -->
        <!-- START row -->
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default" id="demo">
                    <div class="panel-heading">
                        <h3 class="panel-title">Table tools</h3>
                    </div>
                    <table class="table table-striped table-bordered" id="table-tools">
                        <thead>
                            <tr>
                                <th>N°</th>
                                <th>Rôles</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php $i=1; ?>
                        @forelse($roles as $role)
                            <tr>
                                <td>{{$i++}}</td>
                                <td>{{$role->name}}</td>
                                <td>
                                    <a title="Modifier" class="label label-warning" href="{{ route('edit_role', $role->id)}}">
                                        <i class="ico-edit"></i>
                                    </a>
                                    <a title="Supprimer" class="label label-danger" href="javascript:void(0);">
                                        <form action="{{ route('delete_role') }}" method="post">
                                            @csrf
                                            <input type="hidden" name="id" value={{$role->id}} />
                                            <button onclick="return confirm('Voulez-vous vraiment Supprimer ?')" style="background-color: transparent; border: 0px;" type="submit" name="submit">
                                              <i style="color:#fff" class="ico-trash">
                                              </i>
                                            </button>
                                        </form>
                                    </a>
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <th colspan="3">Aucune données</th>
                            </tr>
                        @endforelse
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <!--/ END row -->
    </div>
@endsection