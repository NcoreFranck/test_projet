@extends('layouts.layout')

@section('content')
    <!-- START Template Container -->
    <div class="container-fluid">
        <!-- Page Header -->
        <div class="page-header page-header-block">
            <div class="page-header-section">
                <h4 class="title semibold">Ajouter un Rôle</h4>
            </div>
            <div class="page-header-section">
                <!-- Toolbar -->
                <div class="toolbar clearfix">
                    <div class="col-xs-8">

                    </div>
                    <!-- <div class="col-xs-4">
                        <button class="btn btn-primary pull-right"><i class="ico-plus mr5"></i>Créer</button>
                    </div> -->
                </div>
                <!--/ Toolbar -->
            </div>
        </div>
        <!-- Page Header -->
        <!-- START row -->
        <div class="row">
            <div class="col-md-12">
                <!-- Form default layout -->
                <form class="panel panel-default" method="POST" action="{{$route_action}}">
                    <div class="panel-heading">
                        <h3 class="panel-title">Ajouter un Rôle</h3>
                    </div>
                    {!! csrf_field() !!}
                    <div class="panel-body">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-sm-12">
                                    <label class="control-label">Nom Rôle</label>
                                    <input name="name" type="text" required="" class="form-control" placeholder="Nom Permission" value="{{$role->name}}">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-sm-12">
                                    <label class="control-label">Permissions</label>
                                    <select multiple="multiple" name="permission[]" class="form-control ">
                                        @foreach($permissions as $permission)
                                        <option {{ (in_array($permission->id, $mes_permissions)) ? 'selected' : '' }} value="{{$permission->id}}">{{$permission->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                <div class="panel-footer">
                    <button type="submit" class="btn btn-primary">Enregistrer</button>
                    <a href="javascript:history.back()" class="btn btn-inverse">Annuler</a>
                </div>
            </form>
                    <!--/ Form default layout -->
        </div>
    </div>

@endsection
