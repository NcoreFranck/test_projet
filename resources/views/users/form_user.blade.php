@extends('layouts.layout')

@section('content')
    <!-- START Template Container -->
    <div class="container-fluid">
        <!-- Page Header -->
        <div class="page-header page-header-block">
            <div class="page-header-section">
                <h4 class="title semibold">Ajouter un Utilisateur</h4>
            </div>
            <div class="page-header-section">
                <!-- Toolbar -->
                <div class="toolbar clearfix">
                    <div class="col-xs-8">
                        <select class="form-control text-left" id="selectize-customselect">
                            <option value="0">Display metrics...</option>
                            <option value="1">Last 6 month</option>
                            <option value="2">Last 3 month</option>
                            <option value="3">Last month</option>
                        </select>
                    </div>
                    <!-- <div class="col-xs-4">
                        <button class="btn btn-primary pull-right"><i class="ico-plus mr5"></i>Créer</button>
                    </div> -->
                </div>
                <!--/ Toolbar -->
            </div>
        </div>
        <!-- Page Header -->
        <!-- START row -->
        <div class="row">
            <div class="col-md-12">
                <!-- Form default layout -->
                <form class="panel panel-default" method="POST" action="{{$route_action}}">
                    <div class="panel-heading">
                        <h3 class="panel-title">Ajouter un Utilisateur </h3>
                    </div>
                    {!! csrf_field() !!}
                    <div class="panel-body">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-sm-6">
                                    <label class="control-label">Nom & Prénoms</label>
                                    <input name="name" type="text" required="" class="form-control" placeholder="NOM Prenoms" value="{{$user->name}}">
                                </div>
                                <div class="col-sm-6">
                                    <label class="control-label">Email</label>
                                    <input name="email" placeholder="email@email.com" type="email" required="" class="form-control" value="{{$user->email}}">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-sm-12">
                                    <label class="control-label">Rôle</label>
                                    <select multiple="multiple" name="roles[]" class="form-control ">
                                        @foreach($roles as $roles)
                                        <option {{ (in_array($roles, $mes_roles)) ? 'selected' : '' }} value="{{$roles}}">{{$roles}}</option>
                                        @endforeach

                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="form-group" id="check">
                            <div class="checkbox custom-checkbox">
                                <input type="checkbox" name="gift" id="giftcheckbox" value="1">
                                <label for="giftcheckbox">Editer le Mot de Passe</label>
                            </div>
                        </div>
                        <div class="form-group password_cache" id="password">
                            <div class="row">
                                <div class="col-sm-6 mb10">
                                    <label class="control-label">Mot de Passe</label>
                                    <input name="password" type="password" class="form-control" placeholder="Mot de Passe">
                                </div>
                                <div class="col-sm-6 mb10">
                                    <label class="control-label">Confirmer Mot de Passe</label>
                                    <input name="password_confirmation" type="password" class="form-control" placeholder="Confirmer Mot de Passe">
                                </div>
                            </div>
                        </div>

                    </div>
                <div class="panel-footer">
                    <button type="submit" class="btn btn-primary">Enregistrer</button>
                    <a href="javascript:history.back()" class="btn btn-inverse">Annuler</a>
                </div>
            </form>
                    <!--/ Form default layout -->
        </div>
    </div>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script>
$(document).ready(function () {
$('#password').hide();
//lorsqu'on coche la case
  $('#giftcheckbox').click(function() {
    $('#password').toggle(800);
  });
});
 </script>
@endsection
