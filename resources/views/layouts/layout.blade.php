
<!DOCTYPE html>
<!--
TEMPLATE NAME : Adminre - backend
VERSION : 1.3.0
AUTHOR : JohnPozy
AUTHOR URL : http://themeforest.net/user/JohnPozy
EMAIL : pampersdry@gmail.com
LAST UPDATE: 2015/01/05

** A license must be purchased in order to legally use this template for your project **
** PLEASE SUPPORT ME. YOUR SUPPORT ENSURE THE CONTINUITY OF THIS PROJECT **
-->
<html class="backend">
    <!-- START Head -->
    <head>
        <!-- START META SECTION -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>ADMIN</title>
        <meta name="author" content="pampersdry.info">
        <meta name="description" content="Adminre is a clean and flat backend and frontend theme build with twitter bootstrap">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">

        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="{{asset('temp/image/touch/apple-touch-icon-144x144-precomposed.png')}}">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="{{asset('temp/image/touch/apple-touch-icon-114x114-precomposed.png')}}">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="{{asset('temp/image/touch/apple-touch-icon-72x72-precomposed.png')}}">
        <link rel="apple-touch-icon-precomposed" href="{{asset('temp/image/touch/apple-touch-icon-57x57-precomposed.png')}}">
        <link rel="shortcut icon" href="{{asset('temp/image/favicon.ico')}}">
        <!--/ END META SECTION -->

        <!-- START STYLESHEETS -->
        <link rel="stylesheet" href="{{asset('admin/assets/vendor/select2/select2.css')}}"/>
        <link rel="stylesheet" href="{{asset('admin/assets/vendor/bootstrap-multiselect/bootstrap-multiselect.css')}}"/>
        <!-- Plugins stylesheet : optional -->

        <link href="{{ asset('select/css/fileinput.css')}}" media="all" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" crossorigin="anonymous">
    <link href="{{ asset('select/themes/explorer-fas/theme.css')}}" media="all" rel="stylesheet" type="text/css"/>


        <link rel="stylesheet" href="{{asset('temp/netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css')}}">
        <link rel="stylesheet" href="{{asset('temp/plugins/magnific/css/magnific.css')}}">
        <link rel="stylesheet" href="{{asset('temp/plugins/gritter/css/gritter.css')}}">
        <link rel="stylesheet" href="{{asset('temp/plugins/summernote/css/summernote.css')}}">

        <link rel="stylesheet" href="{{asset('temp/plugins/fileupload/css/fileupload.css')}}">

        <link rel="stylesheet" href="{{asset('temp/plugins/selectize/css/selectize.css')}}">
        <link rel="stylesheet" href="{{asset('temp/plugins/flot/css/flot.css')}}">
        <link rel="stylesheet" href="{{asset('temp/plugins/datatables/css/datatables.css')}}">
        <link rel="stylesheet" href="{{asset('temp/plugins/datatables/css/tabletools.css')}}">

        <link rel="stylesheet" href="{{asset('temp/plugins/touchspin/css/touchspin.css')}}">
        <!--/ Plugins stylesheet : optional -->

        <!-- Application stylesheet : mandatory -->
        <link rel="stylesheet" href="{{asset('temp/stylesheet/bootstrap.css')}}">
        <link rel="stylesheet" href="{{asset('temp/stylesheet/layout.css')}}">
        <link rel="stylesheet" href="{{asset('temp/stylesheet/uielement.css')}}">
        <!--/ Application stylesheet -->

        <!-- Theme stylesheet : optional -->
        <!--/ Theme stylesheet : optional -->

        <!-- modernizr script -->
        <script type="text/javascript" src="{{asset('temp/plugins/modernizr/js/modernizr.js')}}"></script>
        <!--/ modernizr script -->
        <!-- END STYLESHEETS -->

        <!-- GRAPHISME -->
        <script src="https://code.highcharts.com/highcharts.js"></script>
        <script src="https://code.highcharts.com/modules/exporting.js"></script>
        <script src="https://code.highcharts.com/modules/export-data.js"></script>
        <script src="https://code.highcharts.com/modules/accessibility.js"></script>

        <!-- END GRAPHISME -->


    </head>
    @stack('css')
    <!--/ END Head -->

    <!-- START Body -->
    <body>
        <!-- START Template Header -->
        <header id="header" class="navbar">
            <!-- START navbar header -->
            <div style="background:#fff" class="navbar-header">
                <!-- Brand -->
                <a class="navbar-brand" href="{{route('accueil')}}">
                  <span class="" style="color:orange; font-weight: bold;">
                  ADMINISTRATION
                  </span>
                </a>
                <!--/ Brand -->
            </div>
            <!--/ END navbar header -->

            <!-- START Toolbar -->
            <div class="navbar-toolbar clearfix">
                <!-- START Left nav -->
                <ul class="nav navbar-nav navbar-left">
                    <!-- Sidebar shrink -->
                    <li class="hidden-xs hidden-sm">
                        <a href="javascript:void(0);" class="sidebar-minimize" data-toggle="minimize" title="Minimize sidebar">
                            <span class="meta">
                                <span class="icon"></span>
                            </span>
                        </a>
                    </li>
                    <!--/ Sidebar shrink -->

                    <!-- Offcanvas left: This menu will take position at the top of template header (mobile only). Make sure that only #header have the `position: relative`, or it may cause unwanted behavior -->
                    <li class="navbar-main hidden-lg hidden-md hidden-sm">
                        <a href="javascript:void(0);" data-toggle="sidebar" data-direction="ltr" rel="tooltip" title="Menu sidebar">
                            <span class="meta">
                                <span class="icon"><i class="ico-paragraph-justify3"></i></span>
                            </span>
                        </a>
                    </li>
                    <!--/ Offcanvas left -->
                </ul>
                <!--/ END Left nav -->

                <!-- START navbar form -->
                <div class="navbar-form navbar-left dropdown" id="dropdown-form">
                    <form action="" role="search">
                        <div class="has-icon">
                            <input type="text" class="form-control" placeholder="Search application...">
                            <i class="ico-search form-control-icon"></i>
                        </div>
                    </form>
                </div>
                <!-- START navbar form -->

                <!-- START Right nav -->
                <ul class="nav navbar-nav navbar-right">
                    <!-- Profile dropdown -->
                    <li class="dropdown profile">
                        <a href="javascript:void(0);" class="dropdown-toggle dropdown-hover" data-toggle="dropdown">
                            <span class="meta">
                                <span class="avatar"><img src="{{asset('temp/image/avatar/avatar.png')}}" class="img-circle" alt="" /></span>
                                <span class="text hidden-xs hidden-sm pl5">{{ Auth::user()->name}}</span>
                                <span class="caret"></span>
                            </span>
                        </a>
                        <ul class="dropdown-menu" role="menu">
                            <li class="pa15">
                                <h5 class="semibold hidden-xs hidden-sm">
                                    <p class="nm">{{ Auth::user()->name}}</p>
                                    <small class="text-muted"></small>
                                </h5>
                                <h5 class="semibold hidden-md hidden-lg">
                                    <p class="nm">{{ Auth::user()->name}}</p>
                                    <small class="text-muted"></small>
                                </h5>
                            </li>
                            <li class="divider"></li>
                            <li><a href="{{route('profile')}}"><span class="icon"><i class="ico-user"></i></span>Mon Profil</a></li>
                            <li class="divider"></li>
                            <li>
                                <a class="text-danger" href="{{ route('logout') }}"
                                    onclick="event.preventDefault();
                                    document.getElementById('logout-form').submit();">
                                <span class="figure"><i class="ico-exit"></i></span>
                                    <span class="text">Déconnexion</span>
                                </a>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                    @csrf
                                </form>
                            </li>
                        </ul>
                    </li>


                    <!-- Profile dropdown -->

                </ul>
                <!--/ END Right nav -->
            </div>
            <!--/ END Toolbar -->
        </header>
        <!--/ END Template Header -->

        <!-- START Template Sidebar (Left) -->
        <aside class="sidebar sidebar-left sidebar-menu">
            <!-- START Sidebar Content -->
            <section class="content slimscroll">
                <!-- START Template Navigation/Menu -->
                @include('__partials.menu')
                <!--/ END Template Navigation/Menu -->

                <!-- START Sidebar summary -->
                <!--/ END Sidebar summary -->
            </section>
            <!--/ END Sidebar Container -->
        </aside>
        <!--/ END Template Sidebar (Left) -->


        <section id="main" role="main">
        @if (Session::has('flash_message'))
            <div class="container">
                <div class="alert alert-success">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                      {{ Session::get('flash_message') }}
                </div>
            </div>
        @endif

        <div class="container">
            @include('flash::message')
        </div>
            @yield('content')
        </section>

        <!-- START Template Main -->
        <script src="{{asset('admin/assets/vendor/bootstrap-multiselect/bootstrap-multiselect.js')}}"></script>

        <script src="{{asset('admin/assets/vendor/codemirror/addon/selection/active-line.js')}}"></script>



        <script>
          $(function () {
            $('.select2_perso').select2()
          })
        </script>
        <!--/ END Template Main -->

        <!-- START JAVASCRIPT SECTION (Load javascripts at bottom to reduce load time) -->
        <!-- Application and vendor script : mandatory -->
        <script type="text/javascript" src="{{asset('temp/javascript/vendor.js')}}"></script>
        <script type="text/javascript" src="{{asset('temp/javascript/core.js')}}"></script>
        <script type="text/javascript" src="{{asset('temp/javascript/backend/app.js')}}"></script>
        <!--/ Application and vendor script : mandatory -->

        <!-- Plugins and page level script : optional -->
        <script src="{{ asset('select/js/plugins/piexif.js')}}" type="text/javascript"></script>
        <script src="{{ asset('select/js/plugins/sortable.js')}}" type="text/javascript"></script>
        <script src="{{ asset('select/js/fileinput.js')}}" type="text/javascript"></script>
        <script src="{{ asset('select/js/locales/fr.js')}}" type="text/javascript"></script>
        <script src="{{ asset('select/js/locales/es.js')}}" type="text/javascript"></script>
        <script src="{{ asset('select/themes/fas/theme.js')}}" type="text/javascript"></script>
        <script src="{{ asset('select/themes/explorer-fas/theme.js')}}" type="text/javascript"></script>

        <script type="text/javascript" src="{{asset('temp/plugins/magnific/js/jquery.magnific-popup.js')}}"></script>
        <!-- <script type="text/javascript" src="../plugins/selectize/js/selectize.js"></script> -->
        <!-- <script type="text/javascript" src="{{asset('temp/plugins/gritter/js/jquery.gritter.js')}}"></script> -->
        <script type="text/javascript" src="{{asset('temp/plugins/summernote/js/summernote.js')}}"></script>
        <script type="text/javascript" src="{{asset('temp/javascript/backend/pages/email.js')}}"></script>

        <script type="text/javascript" src="{{asset('temp/plugins/fileupload/js/vendor/jquery.ui.widget.js')}}"></script>
        <script type="text/javascript" src="{{asset('temp/plugins/fileupload/js/vendor/load-image.js')}}"></script>
        <script type="text/javascript" src="{{asset('temp/plugins/fileupload/js/vendor/load-image-meta.js')}}"></script>
        <script type="text/javascript" src="{{asset('temp/plugins/fileupload/js/vendor/load-image-exif.js')}}"></script>
        <script type="text/javascript" src="{{asset('temp/plugins/fileupload/js/vendor/load-image-ios.js')}}"></script>
        <script type="text/javascript" src="{{asset('temp/plugins/fileupload/js/vendor/canvas-to-blob.js')}}"></script>
        <script type="text/javascript" src="{{asset('temp/plugins/fileupload/js/vendor/jquery.iframe-transport.js')}}"></script>
        <script type="text/javascript" src="{{asset('temp/plugins/fileupload/js/jquery.fileupload.js')}}"></script>
        <script type="text/javascript" src="{{asset('temp/plugins/fileupload/js/jquery.fileupload-process.js')}}"></script>
        <script type="text/javascript" src="{{asset('temp/plugins/fileupload/js/jquery.fileupload-image.js')}}"></script>
        <script type="text/javascript" src="{{asset('temp/plugins/fileupload/js/jquery.fileupload-audio.js')}}"></script>
        <script type="text/javascript" src="{{asset('temp/plugins/fileupload/js/jquery.fileupload-video.js')}}"></script>
        <script type="text/javascript" src="{{asset('temp/plugins/fileupload/js/jquery.fileupload-validate.js')}}"></script>
        <script type="text/javascript" src="{{asset('temp/plugins/fileupload/js/jquery.fileupload-ui.js')}}"></script>
        <script type="text/javascript" src="{{asset('temp/javascript/backend/forms/fileupload.js')}}"></script>

        <script type="text/javascript" src="{{asset('temp/plugins/selectize/js/selectize.js')}}"></script>
        <script type="text/javascript" src="{{asset('temp/plugins/jquery-ui/js/jquery-ui.js')}}"></script>
        <script type="text/javascript" src="{{asset('temp/plugins/jquery-ui/js/addon/timepicker/jquery-ui-timepicker.js')}}"></script>
        <script type="text/javascript" src="{{asset('temp/plugins/jquery-ui/js/jquery-ui-touch.js')}}"></script>
        <script type="text/javascript" src="{{asset('temp/plugins/inputmask/js/inputmask.js')}}"></script>
        <script type="text/javascript" src="{{asset('temp/plugins/select2/js/select2.js')}}"></script>
        <script type="text/javascript" src="{{asset('temp/plugins/touchspin/js/jquery.bootstrap-touchspin.js')}}"></script>
        <script type="text/javascript" src="{{asset('temp/javascript/backend/forms/element.js')}}"></script>

        <script type="text/javascript" src="{{asset('temp/plugins/datatables/js/jquery.dataTables.js')}}"></script>
        <script type="text/javascript" src="{{asset('temp/plugins/datatables/tabletools/js/dataTables.tableTools.js')}}"></script>
        <script type="text/javascript" src="{{asset('temp/plugins/datatables/js/datatables-bs3.js')}}"></script>
        <script type="text/javascript" src="{{asset('temp/javascript/backend/tables/datatable.js')}}"></script>

        <script type="text/javascript" src="{{asset('temp/plugins/selectize/js/selectize.js')}}"></script>
        <script type="text/javascript" src="{{asset('temp/plugins/flot/js/jquery.flot.js')}}"></script>
        <script type="text/javascript" src="{{asset('temp/plugins/flot/js/jquery.flot.resize.js')}}"></script>
        <script type="text/javascript" src="{{asset('temp/plugins/flot/js/jquery.flot.categories.js')}}"></script>
        <script type="text/javascript" src="{{asset('temp/plugins/flot/js/jquery.flot.time.js')}}"></script>
        <script type="text/javascript" src="{{asset('temp/plugins/flot/js/jquery.flot.tooltip.js')}}"></script>
        <script type="text/javascript" src="{{asset('temp/plugins/flot/js/jquery.flot.spline.js')}}"></script>
        <script type="text/javascript" src="{{asset('temp/javascript/backend/pages/dashboard-v1.js')}}"></script>
        <!--/ Plugins and page level script : optional -->
        <!--/ END JAVASCRIPT SECTION -->


        @livewireScripts
        @stack('scripts')
        @yield('js')
    </body>
    <!--/ END Body -->
</html>
