<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">

    <title>ACTIONS POSEES</title>
    <meta content="" name="description">
    <meta content="" name="keywords">

    <!-- Favicons -->
    <link href="{{asset('front/assets/img/favicon.png')}}" rel="icon">
    <link href="{{asset('front/assets/img/apple-touch-icon.png')}}" rel="apple-touch-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Muli:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

    <!-- Vendor CSS Files -->
    <link href="{{asset('front/assets/vendor/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('front/assets/vendor/icofont/icofont.min.css')}}" rel="stylesheet">
    <link href="{{asset('front/assets/vendor/boxicons/css/boxicons.min.css')}}" rel="stylesheet">
    <link href="{{asset('front/assets/vendor/animate.css/animate.min.css')}}" rel="stylesheet">
    <link href="{{asset('front/assets/vendor/venobox/venobox.css')}}" rel="stylesheet">
    <link href="{{asset('front/assets/vendor/owl.carousel/assets/owl.carousel.min.css')}}" rel="stylesheet">
    <link href="{{asset('front/assets/vendor/aos/aos.css')}}" rel="stylesheet">

    <!-- Template Main CSS File -->
    <link href="{{asset('front/assets/css/style.css')}}" rel="stylesheet">

    <!-- =======================================================
    * Template Name: Flattern - v2.2.1
    * Template URL: https://bootstrapmade.com/flattern-multipurpose-bootstrap-template/
    * Author: BootstrapMade.com
    * License: https://bootstrapmade.com/license/
    ======================================================== -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <style media="screen">
        li a {
            font-weight: bold;
        }
        .form-control_perso {
            display: block;
            width: 100%;
            height: 27px;
            padding: .200rem .45rem;
            font-size: 12px;
            font-weight: 400;
            line-height: 1;
            color: #495057;
            background-color: #fff;
            background-clip: padding-box;
            border: 1px solid #ced4da;
            border-radius: .25rem;
            transition: border-color .15s ease-in-out,box-shadow .15s ease-in-out;
        }
        label {
            font-size: 13px;
        }
        .bbtn {
            background:#E8F0FE;
            border:1px solid gray;
            opacity:0,8;
            background-clip: padding-box;
            border:none;
            border-radius: .25rem;
            transition: border-color .15s;
            padding-left: 12px;
            padding-right: 12px;
        }
        .groupe_inpval {
            display:flex;
            flex-flow:row nowrap;
            border: 1px solid #ced4da;
            border-radius:6px;
        }
        .show-password {
            text-transform: uppercase;
            cursor: pointer;
        }
        .mdep {
            display:flex;
            flex-flow:row nowrap;
            border: 1px solid #ced4da;
            border-radius:6px;
        }
        body {
            font-family: cursive;
        }
        .selector_star_check {
            color: orange;
        }
        .Radio {
            display: inline-flex;
            align-items: center;
        }

        .Radio--large {
            font-size: 2em;
        }

        .Radio-Input {
            margin: 0 0.5em 0;
        }
    </style>
    @yield('css')
</head>

<body>

<!-- ======= Top Bar ======= -->
<section id="topbar" class="d-none d-lg-block">
    <div class="container d-flex">
        <div class="contact-info mr-auto">
            <i class="icofont-envelope"></i><a href="mailto:info@ongbloom.com">junoserge@yahoo.fr</a>
            <i class="icofont-phone"></i>  (+225) 07 0741 8847
        </div>
        <div class="social-links">
            <a href="#" class="twitter"><i class="icofont-twitter"></i></a>
            <a href="#" class="facebook"><i class="icofont-facebook"></i></a>
            <a href="#" class="instagram"><i class="icofont-instagram"></i></a>
            <a href="#" class="skype"><i class="icofont-skype"></i></a>
            <a href="#" class="linkedin"><i class="icofont-linkedin"></i></a>
        </div>
    </div>
</section>

<!-- ======= Header ======= -->
<header id="header">
    <div class="container d-flex">

        <div class="logo mr-auto">
            <h1 class="text-light"><a href="{{route('accueil')}}"><b>J'<i style="color:red" class="fa fa-heart" aria-hidden="true"></i> COCODY</b>, la commune des CHAMPIONs</a></h1>
        </div>

        <nav class="nav-menu d-none d-lg-block">
            <ul>
                <li><a href="{{route('login')}}">Connexion</a></li>
            </ul>
        </nav><!-- .nav-menu -->

    </div>
</header><!-- End Header -->
<!-- ======= Hero Section ======= -->
<div class="container">
    <div class="row">
        <div class="col-lg-12">
            @if (session()->has('message'))
                <div class="alert alert-success">
                    {{ session('message') }}
                </div>
            @endif

            <div class="col-lg-12">
                @if (session()->has('message_error'))
                    <div class="alert alert-danger">
                        {{ session('message_error') }}
                    </div>
                @endif
            </div>
        </div>
    </div>
</div>

@yield('content')

<!-- ======= Footer ======= -->
<footer id="footer">
    <div class="container d-md-flex py-4">

        <div class="mr-md-auto text-center text-md-left">
            <div class="copyright">
                &copy; Copyright <strong><span>Actions</span></strong>. All Rights Reserved
            </div>
            <div class="credits">

            </div>
        </div>
        <div class="social-links text-center text-md-right pt-3 pt-md-0">
            <a href="#" class="twitter"><i class="bx bxl-twitter"></i></a>
            <a href="#" class="facebook"><i class="bx bxl-facebook"></i></a>
            <a href="#" class="instagram"><i class="bx bxl-instagram"></i></a>
            <a href="#" class="google-plus"><i class="bx bxl-skype"></i></a>
            <a href="#" class="linkedin"><i class="bx bxl-linkedin"></i></a>
        </div>
    </div>
</footer><!-- End Footer -->

<a href="#" class="back-to-top"><i class="icofont-simple-up"></i></a>

<!-- Vendor JS Files -->
<script src="{{asset('front/assets/vendor/jquery/jquery.min.js')}}"></script>
<script src="{{asset('front/assets/vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
<script src="{{asset('front/assets/vendor/jquery.easing/jquery.easing.min.js')}}"></script>
<script src="{{asset('front/assets/vendor/php-email-form/validate.js')}}"></script>
<script src="{{asset('front/assets/vendor/jquery-sticky/jquery.sticky.js')}}"></script>
<script src="{{asset('front/assets/vendor/isotope-layout/isotope.pkgd.min.js')}}"></script>
<script src="{{asset('front/assets/vendor/venobox/venobox.min.js')}}"></script>
<script src="{{asset('front/assets/vendor/waypoints/jquery.waypoints.min.js')}}"></script>
<script src="{{asset('front/assets/vendor/owl.carousel/owl.carousel.min.js')}}"></script>
<script src="{{asset('front/assets/vendor/aos/aos.js')}}"></script>

<!-- Template Main JS File -->
<script src="{{asset('front/assets/js/main.js')}}"></script>

<!-- debut Select2 files -->
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
<!-- fin Select2 files -->
<script type="text/javascript">
    $(document).ready(function(){
        $('.show-password').click(function() {
            if($(this).prev('input').prop('type') == 'password') {
                //Si c'est un input type password
                $(this).prev('input').prop('type','text');
                $(this).addClass("fa fa-eye-slash");
            } else {
                //Sinon
                $(this).prev('input').prop('type','password');
                $(this).removeClass("fa fa-eye-slash");
                $(this).addClass("fa fa-eye");
            }
        });

    });
</script>

@yield('js')
</body>

</html>
