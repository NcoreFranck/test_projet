@extends('layouts.layout')

@section('content')
<!-- START Template Container -->
<div class="container-fluid">
  <!-- Page Header -->
  <div class="page-header page-header-block">
    <div class="page-header-section">
      <h4 class="title semibold">Partenaires</h4>
    </div>
    <div class="page-header-section">
      <!-- Toolbar -->
      <div class="toolbar clearfix">
        <div class="col-xs-8">
        </div>
        <div class="col-xs-4">
          <a class="btn btn-primary pull-right" href="{{route('param.partenaire_add')}}"><i class="ico-settings mr5"></i>Ajouter</a>
        </div>
      </div>
      <!--/ Toolbar -->
    </div>
  </div>
  <!-- Page Header -->
  <!-- START row -->
  <div style="font-size:9px" class="row">
    <div class="col-md-12">
      <div class="panel panel-default" id="demo">

        <div style="font-size:14px" class="col-lg-12">
          <form method="post" action="{{$add_update}}" enctype="multipart/form-data" class="form-horizontal">
            {!! csrf_field() !!}
            <br>
            <div class="col-lg-12">
              @if($partenaire->logo != '')
              <br>
              <img src="{{asset($partenaire->logo)}}" class="img-fluid responsive" width="20%" alt="">
              <br><br>
              @endif
            </div>

            <div class="col-lg-6">
              <label>Logo <span style="color:red">*</span></label>
              <input class="form-control" type="file" name="logo" value="">
              @error('logo') <span class="text-danger">{{ $message }}</span> @enderror
            </div>

            <div class="col-lg-6">
              <label>Site internet <span style="font-size:10px" > (EX: https://nom_site_internet.com ou http://nom_site_internet.com)</span> </label>
              <input class="form-control" type="text" value="{{$partenaire->site_internet}}" name="site_internet" value="">
              @error('site_internet') <span class="text-danger">{{ $message }}</span> @enderror
            </div>

            <div class="col-lg-12"><br>
              <label>Nom de l'entreprise <span style="color:red">*</span></label>
              <input required class="form-control" type="text" value="{{$partenaire->entreprise_nom}}" name="entreprise_nom" value="">
              @error('entreprise_nom') <span class="text-danger">{{ $message }}</span> @enderror
            </div>

            <div class="col-lg-12"><br>
              <label>Vidéo (Integration Youtube)</label>
              <textarea name="lien_video" class="form-control" rows="2" cols="80">{{$partenaire->lien_video}}</textarea>
              @error('lien_video') <span class="text-danger">{{ $message }}</span> @enderror
            </div>

            <div class="col-lg-12">
              <br>
              <label>Description <span style="color:red">*</span></label>
              <textarea required name="description" class="form-control about" rows="4" cols="80">{{$partenaire->description}}</textarea>
              @error('description') <span class="text-danger">{{ $message }}</span> @enderror
            </div>

            <div class="col-lg-12">
              <br>
              <input type="hidden" name="partenaire_id" value="{{$partenaire->id}}">
              <button type="submit" class="btn btn-success btn-sm">ENREGISTRER</button>
            </div>
            </form>
          </div>


          <div class="row">
          </div>


        <br><br>
      </div>

    </div>
  </div>
</div>
<!--/ END row -->
@endsection


@section('js')
<script src="{{ asset('assets/js/plugins/ckeditor/ckeditor.js')}}"></script>
<script>
CKEDITOR.replace( 'summary-ckeditor' );
CKEDITOR.replace( 'js-ckeditor1' );
CKEDITOR.replace( 'js-ckeditor2' );
CKEDITOR.replace( 'js-ckeditor3' );
CKEDITOR.replace( 'js-ckeditor4' );
CKEDITOR.replace( 'js-ckeditor5' );
CKEDITOR.replace( 'js-ckeditor6' );
CKEDITOR.replace( 'js-ckeditor7' );
CKEDITOR.replaceAll('about');
</script>
<script>
    jQuery(function(){ Codebase.helpers(['flatpickr', 'datepicker', 'colorpicker', 'maxlength', 'select2', 'masked-inputs', 'rangeslider', 'tags-inputs', 'summernote', 'ckeditor', 'simplemde']); });

</script>

<script>
function goBack() {
  window.history.back();
}
</script>
@endsection
