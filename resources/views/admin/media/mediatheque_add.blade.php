@extends('layouts.layout')

@section('content')
    <!-- START Template Container -->
    <div class="container-fluid">
        <!-- Page Header -->
        <div class="page-header page-header-block">
            <div class="page-header-section">
                <h4 class="title semibold">Publication</h4>
            </div>
            <div class="page-header-section">
                <!-- Toolbar -->
                <div class="toolbar clearfix">
                    <div class="col-xs-8">
                    </div>
                </div>
                <!--/ Toolbar -->
            </div>
        </div>
        <!-- Page Header -->
        <!-- START row -->
        <div style="font-size:9px" class="row">
            <div class="col-md-12">
                <div class="panel panel-default" id="demo">

                    <div style="font-size:14px" class="col-lg-12">
                        <form method="post" action="{{$add_update}}" enctype="multipart/form-data"
                              class="form-horizontal">
                            {!! csrf_field() !!}
                            <br>

                            <div class="col-lg-12"><br>
                                <label>Titre <span style="color:red">*</span></label>
                                <input required class="form-control" type="text" value="{{$media->libelle}}"
                                       name="libelle" value="">
                                @error('libelle') <span class="text-danger">{{ $message }}</span> @enderror
                            </div>

                            <div class="col-lg-12"><br>
                                <label>Image d'illustration <span style="color:red">*</span></label>
                                <input class="form-control" type="file" name="source" value="">
                                @error('source') <span class="text-danger">{{ $message }}</span> @enderror
                                @if($media->source != '')
                                    <br>
                                    <img src="{{asset($media->source)}}" class="img-fluid responsive" width="30%"
                                         alt="">
                                    <br><br>
                                @endif
                            </div>

                            <div class="col-lg-12"><br>
                                <label>Description <span style="color:red">*</span></label>
                                <textarea required name="description" class="form-control about" rows="4"
                                          cols="80">{{$media->description}}</textarea>
                                @error('description') <span class="text-danger">{{ $message }}</span> @enderror
                            </div>

                            <div class="col-lg-12"><br>
                                <label>Tags <span style="color:red">*</span></label>
                                <select multiple="multiple" name="tags[]" class="form-control ">
                                    @foreach($tags as $tag)
                                        <option {{ (in_array($tag->id, $mes_tags)) ? 'selected' : '' }} value="{{$tag->id}}">{{$tag->libelle}}</option>
                                    @endforeach
                                </select>
                                @error('tags') <span class="text-danger">{{ $message }}</span> @enderror
                            </div>

                            <div class="col-lg-12">
                                <br><br><br>
                                <input type="hidden" name="media_id" value="{{$media->id}}">
                                <button style="margin-bottom:60px" type="submit" class="btn btn-success btn-sm">
                                    ENREGISTRER
                                </button>
                            </div>
                        </form>
                    </div>
                    <div class="row">

                    </div>

                </div>

            </div>
        </div>
    </div>




    <!--/ END row -->
@endsection

@section('js')
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <script src="{{ asset('assets/js/plugins/ckeditor/ckeditor.js')}}"></script>
    <script>
        CKEDITOR.replace('summary-ckeditor');
        CKEDITOR.replace('js-ckeditor1');
        CKEDITOR.replace('js-ckeditor2');
        CKEDITOR.replace('js-ckeditor3');
        CKEDITOR.replace('js-ckeditor4');
        CKEDITOR.replace('js-ckeditor5');
        CKEDITOR.replace('js-ckeditor6');
        CKEDITOR.replace('js-ckeditor7');
        CKEDITOR.replaceAll('about');
    </script>
    <script>
        jQuery(function () {
            Codebase.helpers(['flatpickr', 'datepicker', 'colorpicker', 'maxlength', 'select2', 'masked-inputs', 'rangeslider', 'tags-inputs', 'summernote', 'ckeditor', 'simplemde']);
        });

    </script>
@endsection
