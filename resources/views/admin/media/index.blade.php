@extends('layouts.layout')

@section('content')
    <!-- START Template Container -->
    <div class="container-fluid">
        <!-- Page Header -->
        <div class="page-header page-header-block">
            <div class="page-header-section">
                <h4 class="title semibold">Liste des publications</h4>
            </div>
            <div class="page-header-section">
                <!-- Toolbar -->
                <div class="toolbar clearfix">
                    <div class="col-xs-8">
                    </div>
                    <div class="col-xs-4">
                        <a class="btn btn-primary pull-right" href="{{route('admin.mediatheque_add')}}"><i
                                class="ico-settings mr5"></i>Ajouter</a>
                    </div>
                </div>
                <!--/ Toolbar -->
            </div>
        </div>
        <!-- Page Header -->
        <!-- START row -->
        <div style="font-size:9px" class="row">
            <div class="col-md-12">
                <div class="panel panel-default" id="demo">
                    <div class="panel-heading">
                        <h3 class="panel-title"></h3>
                    </div>
                    <table class="table table-striped table-bordered" id="table-tools">
                        <thead>
                        <tr>
                            <th width="3%">N°</th>
                            <th width="10%">Image</th>
                            <th>Titre</th>
                            <th>Tags</th>
                            <th width="25%">Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php $i = 1; ?>
                        @foreach($medias as $media)
                            <tr>
                                <td>{{$i++}}</td>
                                <td align="center">
                                    <img src="{{asset($media->source)}}" class="img-fluid responsive" width="100%"
                                         alt="">
                                </td>
                                <td>{{$media->libelle}}</td>
                                <td>
                                    @foreach($media->media_get_tags as $tag)
                                        <span style="padding: 8px" class="badge-info">{{$tag->libelle}}</span>
                                    @endforeach
                                </td>
                                <td>
                                    <a class="btn btn-xs btn-primary"
                                       href="{{route('admin.mediatheque_edit', $media->id)}}">
                                        <i style="color:#fff" class="ico-pencil mr5"></i> Editer
                                    </a>

                                    <a class="btn btn-xs btn-danger" href="" style="color:purple">
                                        <form action="{{ route('admin.mediatheque_delete') }}" method="post">
                                            @csrf
                                            <input type="hidden" name="id" value="{{$media->id}}"/>
                                            <button onclick="return confirm('Voulez-vous vraiment Supprimer ?')"
                                                    class="btn btn-xs btn-primary"
                                                    style="background-color: transparent; border: 0px;" type="submit"
                                                    name="submit">
                                                <i style="color:#000" class="fa fa-mobitrashle" aria-hidden="true"></i>
                                                Supprimer
                                            </button>
                                        </form>
                                    </a>
                                </td>

                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <!--/ END row -->
    </div>
@endsection
