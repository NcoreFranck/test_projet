@extends('layouts.layout')

@section('content')
<!-- START Template Container -->
<div class="container-fluid">
  <!-- Page Header -->
  <div class="page-header page-header-block">
    <div class="page-header-section">
      <h4 class="title semibold">Membre</h4>
    </div>
    <div class="page-header-section">
      <!-- Toolbar -->
      <div class="toolbar clearfix">
        <div class="col-xs-8">
        </div>
        <div class="col-xs-4">
        </div>
      </div>
      <!--/ Toolbar -->
    </div>
  </div>
  <!-- Page Header -->
  <!-- START row -->
  <div class="row">

      <div class="col-lg-12">

          <div class="row" style="background: #fff; margin: 20px" >
              <form method="post" action="{{ route('admin.membres_inscrit_save') }}" enctype="multipart/form-data" class="form-horizontal">
                  {!! csrf_field() !!}
                  <div align="center" class="col-lg-12">
                      <label align="center" style="font-size: 25px"> <b>Avez-vous un projet que vous souhaitez réaliser ? <span style="color:red">*</span></b></label>
                      <div>
                          <label style="font-size: 25px" class="Radio" for="oui-projet">
                              <input class="Radio-Input" type="radio" id="oui-projet" name="onliste_porteur_projet" {{ ($membre_inscrit->onliste_porteur_projet == 'oui') ? 'checked' : '' }} value="oui" />
                              Oui
                          </label>

                          <label style="font-size: 25px" class="Radio" for="non-projet">
                              <input class="Radio-Input" type="radio" id="non-projet" name="onliste_porteur_projet" {{ ($membre_inscrit->onliste_porteur_projet == 'non') ? 'checked' : '' }} value="non" />
                              Non
                          </label>
                      </div>
                      @error('onliste_porteur_projet') <span class="text-danger">{{ $message }}</span> @enderror
                  </div>

                  <div class="col-lg-12">
                      <label>Commune <span style="color:red">*</span></label>
                      <select required class="form-control" name="ville_commune">
                          <option value=""> .:: Sélectionner ::.</option>
                          @foreach($ville_communes as $ville_commune)
                              <option {{ ($membre_inscrit->ville_commune == $ville_commune->id) ? 'selected' : '' }} value="{{$ville_commune->id}}"> {{$ville_commune->libelle}}</option>
                          @endforeach
                      </select>
                      @error('commune') <span
                          class="text-danger">{{ $message }}</span> @enderror
                  </div>

                  <div class="col-lg-6">
                      <label>Nom <span style="color:red">*</span></label>
                      <input required class="form-control" type="text" value="{{$membre_inscrit->nom}}" name="nom">
                      @error('nom') <span
                          class="text-danger">{{ $message }}</span> @enderror
                  </div>

                  <div class="col-lg-6">
                      <label>Prénoms <span style="color:red">*</span></label>
                      <input required class="form-control" type="text" value="{{$membre_inscrit->prenoms}}"
                             name="prenoms">
                      @error('prenoms') <span
                          class="text-danger">{{ $message }}</span> @enderror
                  </div>

                  <div class="col-lg-6">
                      <label>Numéro de téléphone 1 <span style="color:red">*</span></label>
                      <input required class="form-control" type="text"
                             name="tel" value="{{$membre_inscrit->tel}}">
                      @error('tel') <span
                          class="text-danger">{{ $message }}</span> @enderror
                  </div>

                  <div class="col-lg-6">
                      <label>Numéro de téléphone 2</label>
                      <input class="form-control" type="text"
                             name="tel2" value="{{$membre_inscrit->tel2}}">
                      @error('tel2') <span
                          class="text-danger">{{ $message }}</span> @enderror
                  </div>

                  <div class="col-lg-6">
                      <label>Email</label>
                      <input class="form-control" type="email"
                             name="email" value="{{$membre_inscrit->email}}">
                      @error('email') <span
                          class="text-danger">{{ $message }}</span> @enderror
                  </div>

                  <div class="col-lg-6">
                      <label>Numéro Carte Nationale d'Identité</label>
                      <input class="form-control" type="numero_cni"
                             name="numero_cni" value="{{$membre_inscrit->numero_cni}}">
                      @error('numero_cni') <span
                          class="text-danger">{{ $message }}</span> @enderror
                  </div>

                  <div class="col-lg-6">
                      <label>Numéro Carte d'Électeur</label>
                      <input class="form-control" type="numero_ce"
                             name="numero_ce" value="{{$membre_inscrit->numero_ce}}">
                      @error('numero_ce') <span
                          class="text-danger">{{ $message }}</span> @enderror
                  </div>

                  <div style="padding-bottom: 10px" class="col-lg-6">
                      <label>Votre date de naissance <span style="color:red">*</span></label>
                      <input class="form-control" type="date"
                             name="date_naiss" value="{{$membre_inscrit->date_naiss}}">
                      @error('date_naiss') <span
                          class="text-danger">{{ $message }}</span> @enderror
                  </div>

                  <div class="col-lg-6">
                      <label>Genre <span style="color:red">*</span></label>
                      <select required class="form-control" name="sexe">
                          <option value=""> .:: Sélectionner ::.</option>
                          <option {{ ($membre_inscrit->sexe == 'MASCULIN') ? 'selected' : '' }} value="MASCULIN"> MASCULIN</option>
                          <option {{ ($membre_inscrit->sexe == 'FEMININ') ? 'selected' : '' }} value="FEMININ"> FEMININ</option>
                      </select>
                      @error('sexe') <span
                          class="text-danger">{{ $message }}</span> @enderror
                  </div>

                  <div class="col-lg-6">
                      <label>Activités <span style="color:red">*</span></label>
                      <select required class="form-control" name="activites">
                          <option value=""> .:: Sélectionner ::.</option>
                          <option {{ ($membre_inscrit->activites == 'ÉLEVE') ? 'selected' : '' }} value="ÉLEVE"> ÉLÈVE</option>
                          <option {{ ($membre_inscrit->activites == 'ÉTUDIANT') ? 'selected' : '' }} value="ÉTUDIANT"> ÉTUDIANT(E)</option>
                          <option {{ ($membre_inscrit->activites == 'COMMERCANT') ? 'selected' : '' }} value="COMMERCANT"> COMMERCANT(E)</option>
                          <option {{ ($membre_inscrit->activites == 'FONCTIONNAIRE') ? 'selected' : '' }} value="FONCTIONNAIRE"> FONCTIONNAIRE</option>
                          <option {{ ($membre_inscrit->activites == 'CONTRACTUEL') ? 'selected' : '' }} value="CONTRACTUEL"> CONTRACTUEL(LE)</option>
                          <option {{ ($membre_inscrit->activites == "RECHERCHE D'EMPLOI") ? 'selected' : '' }} value="RECHERCHE D'EMPLOI"> RECHERCHE D'EMPLOI</option>
                      </select>
                      @error('activites') <span
                          class="text-danger">{{ $message }}</span> @enderror
                  </div>

                  <div class="col-lg-12"><br>
                      <label>Êtes-vous inscrit(e) sur une liste électorale à Cocody ? <span style="color:red">*</span></label>
                      <div>
                          <label class="Radio" for="oui">
                              <input class="Radio-Input" type="radio" id="oui" name="onliste_elect" {{ ($membre_inscrit->onliste_elect == 'oui') ? 'checked' : '' }} value="oui" />
                              Oui
                          </label>

                          <label class="Radio" for="non">
                              <input class="Radio-Input" type="radio" id="non" name="onliste_elect" {{ ($membre_inscrit->onliste_elect == 'non') ? 'checked' : '' }} value="non" />
                              Non
                          </label>
                      </div>
                      @error('numero_cni') <span class="text-danger">{{ $message }}</span> @enderror
                  </div>

                  <input type="hidden" name="_membre_id" value="{{$membre_inscrit->id}}">
                  <div class="col-lg-12 text-center">
                      <button style="font-size: 14px" type="submit" class="btn btn-success align-middle"> <b>ENREGISTRER LES MODIFICATIONS</b>
                      </button>
                      <br><br>
                      <br><br>
                  </div>
              </form>
          </div>

      </div>

  </div>
</div>
<!--/ END row -->
@endsection

@section('js')
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <script src="{{ asset('assets/js/plugins/ckeditor/ckeditor.js')}}"></script>
    <script>
        CKEDITOR.replace('summary-ckeditor');
        CKEDITOR.replace('js-ckeditor1');
        CKEDITOR.replace('js-ckeditor2');
        CKEDITOR.replace('js-ckeditor3');
        CKEDITOR.replace('js-ckeditor4');
        CKEDITOR.replace('js-ckeditor5');
        CKEDITOR.replace('js-ckeditor6');
        CKEDITOR.replace('js-ckeditor7');
        CKEDITOR.replaceAll('about');
    </script>
    <script>
        jQuery(function () {
            Codebase.helpers(['flatpickr', 'datepicker', 'colorpicker', 'maxlength', 'select2', 'masked-inputs', 'rangeslider', 'tags-inputs', 'summernote', 'ckeditor', 'simplemde']);
        });

    </script>
@endsection
