@extends('layouts.layout')

@section('content')
    <!-- START Template Container -->
    <div class="container-fluid">
        <!-- Page Header -->
        <div class="page-header page-header-block">
            <div class="page-header-section">
                <h4 class="title semibold">Liste des Membres</h4>
            </div>
            <div class="page-header-section">
                <!-- Toolbar -->
                <div class="toolbar clearfix">
                    <div class="col-xs-4">
                    </div>
                    <div align="right" class="col-xs-8">
                        <a class="btn btn-warning pull-right" href="{{route('admin.membres_inscrits_print', 'pdf')}}"><i class="fa fa-file-pdf"></i> Imprimer en PDF</a>
                        <a style="margin-right: 10px" class="btn btn-success pull-right" href="{{route('admin.membres_inscrits_print', 'excel')}}"><i class="fa fa-file-excel"></i> Imprimer en EXCEL</a>
                    </div>
                </div>
                <!--/ Toolbar -->
            </div>
        </div>
        <!-- Page Header -->
        <!-- START row -->
        <div style="font-size:9px" class="row">
            <div class="col-md-12">
                <div class="panel panel-default" id="demo">
                    <div class="panel-heading">
                        <h3 class="panel-title">
                            {{$membres_inscrits->count()}} Membre(s)
                        </h3>
                    </div>
                    <table class="table table-striped table-bordered" id="table-tools">
                        <thead>
                        <tr>
                            <th width="3%">N°</th>
                            <th>Date d'inscription</th>
                            <th>Nom</th>
                            <th>Prénoms</th>
                            <th>Sexe</th>
                            <th>Tel 1</th>
                            <th>Tel 2</th>
                            <th>Numéro CNI</th>
                            <th>Numéro CE</th>
                            <th>Date Naissance</th>
                            <th>Activité</th>
                            <th>Inscrit sur la liste ?</th>
                            <th>Porteur de Projet</th>
                            <th width="15%">Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php $i = 1; ?>
                        @foreach($membres_inscrits as $membres_inscrit)
                            <tr>
                                <td>{{$i++}}</td>
                                <td>{{date('d-m-Y H:i', strtotime($membres_inscrit->created_at))}}</td>
                                <td>{{$membres_inscrit->nom}}</td>
                                <td>{{$membres_inscrit->prenoms}}</td>
                                <td>{{$membres_inscrit->sexe}}</td>
                                <td>{{$membres_inscrit->tel}}</td>
                                <td>{{$membres_inscrit->tel1}}</td>
                                <td>{{$membres_inscrit->numero_cni}}</td>
                                <td>{{$membres_inscrit->numero_ce}}</td>
                                <td>{{date('d-m-Y', strtotime($membres_inscrit->date_naiss))}}</td>
                                <td>{{$membres_inscrit->activites}}</td>
                                <td>{{$membres_inscrit->onliste_elect}}</td>
                                <td>{{$membres_inscrit->onliste_porteur_projet}}</td>
                                <td>
                                    <a class="btn btn-xs btn-primary" href="{{route('admin.membres_inscrit_edit', $membres_inscrit->id)}}">
                                        <i style="color:#fff" class="ico-pencil mr5"></i> Editer
                                    </a>

                                    <a class="btn btn-xs btn-danger" href="" style="color:purple">
                                        <form action="{{ route('admin.membres_inscrit_delete') }}" method="post">
                                            @csrf
                                            <input type="hidden" name="id" value="{{$membres_inscrit->id}}" />
                                            <button onclick="return confirm('Voulez-vous vraiment Supprimer ?')" class="btn btn-xs btn-primary" style="background-color: transparent; border: 0px;" type="submit" name="submit">
                                                <i style="color:#000" class="fa fa-mobitrashle" aria-hidden="true"></i> Supprimer
                                            </button>
                                        </form>
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <!--/ END row -->
    </div>
@endsection
