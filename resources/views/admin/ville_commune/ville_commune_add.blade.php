@extends('layouts.layout')

@section('content')
<!-- START Template Container -->
<div class="container-fluid">
  <!-- Page Header -->
  <div class="page-header page-header-block">
    <div class="page-header-section">
      <h4 class="title semibold">Liste des villees/communes</h4>
    </div>
    <div class="page-header-section">
      <!-- Toolbar -->
      <div class="toolbar clearfix">
        <div class="col-xs-8">
        </div>
        <div class="col-xs-4">
          <a class="btn btn-primary pull-right" href="{{route('param.ville_commune_add')}}"><i class="ico-settings mr5"></i>Ajouter</a>
        </div>
      </div>
      <!--/ Toolbar -->
    </div>
  </div>
  <!-- Page Header -->
  <!-- START row -->
  <div style="font-size:9px" class="row">
    <div class="col-md-12">
      <div class="panel panel-default" id="demo">

        <div style="font-size:14px" class="col-lg-12">
          <form method="post" action="{{$add_update}}" enctype="multipart/form-data" class="form-horizontal">
            {!! csrf_field() !!}
            <br>

            <div class="col-lg-12">
              <label>Libellé <span style="color:red">*</span></label>
              <input required class="form-control" type="text" value="{{$ville_commune->libelle}}" name="libelle">
              @error('libelle') <span class="text-danger">{{ $message }}</span> @enderror
            </div>

            <div class="col-lg-12">
              <br>
              <input type="hidden" name="ville_commune_id" value="{{$ville_commune->id}}">
              <button type="submit" class="btn btn-success btn-sm">ENREGISTRER</button>
            </div>
            </form>
          </div>


          <div class="row">
          </div>


        <br><br>
      </div>

    </div>
  </div>
</div>
<!--/ END row -->
@endsection
