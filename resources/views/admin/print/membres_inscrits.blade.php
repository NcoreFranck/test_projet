<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>ATTESTATION</title>

    <style>
        html, body {
            font-family: 'Arial', sans-serif;
        }

        table tr {
            padding-top: 10px;
        }

        table tr td {
            padding: 10px;
        }

        table tr th {
            padding: 10px;
        }

        table {
            width: 100%;
        }

    </style>

</head>
<body>

<table border="" width="100%" cellspacing="0" cellpadding="0">
    <tbody>

    <tr style="font-size: 14px; line-height:5px">
        <td style="" align="right" colspan="8">
            <p>{{ $date_impression }}</p>
        </td>
    </tr>
    <tr>
        <td style="text-align:center;" colspan="8" width="100%">
            <h3>LISTE DES MEMBRES</h3>
        </td>
    </tr>
    </tbody>
</table>


<table border="1" style="width: 100%" cellspacing="0" cellpadding="0">
    <thead style="font-size:9px" align="left">
    <tr>
        <th>N°</th>
        <th>Nom</th>
        <th>Prénoms</th>
        <th>Date de naissance</th>
        <th>Téléphone 1</th>
        <th>Téléphone 2</th>
        <th>Numéro CNI</th>
        <th>Numéro CE</th>
        <th>Commune</th>
    </tr>
    </thead>

    <tbody style="font-size:9px">
    <?php $total = 0; ?>
    @foreach($membres_inscrits as $user)
        <tr>
            <td>{{$loop->iteration}}</td>
            <td>{{$user->nom}}</td>
            <td>{{$user->prenoms}}</td>
            <td>{{date('d-m-Y', strtotime($user->date_naiss))}}</td>
            <td>{{$user->tel}}</td>
            <td>{{$user->tel1}}</td>
            <td>{{$user->numero_cni}}</td>
            <td>{{$user->numero_ce}}</td>
            <td>{{($user->membre_get_commune_ville == null) ? '' : $user->membre_get_commune_ville->libelle}}</td>
        </tr>
    @endforeach
    <tr>
        <td colspan="8"><b>TOTAL</b> </td>
        <td align="right"><b> {{$membres_inscrits->count()}}</b> </td>
    </tr>

    </tbody>
</table>


</body>
</html>
