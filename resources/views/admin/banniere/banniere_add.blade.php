@extends('layouts.layout')

@section('content')
<!-- START Template Container -->
<div class="container-fluid">
  <!-- Page Header -->
  <div class="page-header page-header-block">
    <div class="page-header-section">
      <h4 class="title semibold">Liste des Bannières</h4>
    </div>
    <div class="page-header-section">
      <!-- Toolbar -->
      <div class="toolbar clearfix">
        <div class="col-xs-8">
        </div>
        <div class="col-xs-4">
          <a class="btn btn-primary pull-right" href="{{route('param.banniere_add')}}"><i class="ico-settings mr5"></i>Ajouter</a>
        </div>
      </div>
      <!--/ Toolbar -->
    </div>
  </div>
  <!-- Page Header -->
  <!-- START row -->
  <div style="font-size:9px" class="row">
    <div class="col-md-12">
      <div class="panel panel-default" id="demo">

        <div style="font-size:14px" class="col-lg-12">
          <form method="post" action="{{$add_update}}" enctype="multipart/form-data" class="form-horizontal">
            {!! csrf_field() !!}
            <br>

            <div class="col-lg-12">
              <label>Titre <span style="color:red">*</span></label>
              <input required class="form-control" type="text" value="{{$banniere->titre}}" name="titre" value="">
              @error('titre') <span class="text-danger">{{ $message }}</span> @enderror
            </div>

            <div class="col-lg-12">
                <br>
              <label>Charger la bannière <span style="color:red">*</span></label>
              <input class="form-control" type="file" name="source" value="">
              @error('source') <span class="text-danger">{{ $message }}</span> @enderror
              @if($banniere->source != '')
              <br>
              <img src="{{asset($banniere->source)}}" class="img-fluid responsive" width="30%" alt="">
              <br><br>
              @endif
            </div>

            <div class="col-lg-12">
                <br>
              <label>Description <span style="color:red">*</span></label>
              <textarea required name="description" class="form-control" rows="4" cols="80">{{$banniere->description}}</textarea>
              @error('description') <span class="text-danger">{{ $message }}</span> @enderror
            </div>

              <div class="col-lg-12">
                  <br>
                  <label>Description <span style="color:red">*</span></label>
                  <textarea required name="autre_info" class="form-control about" rows="10" cols="80">{{$banniere->autre_info}}</textarea>
                  @error('autre_info') <span class="text-danger">{{ $message }}</span> @enderror
              </div>

            <div class="col-lg-12">
                <br>
              <label>Etat</label>
              <select class="form-control" name="etat">
                <option value=""> -- Selectionnez --</option>
                <option {{ ($banniere->etat == 'active') ? 'selected' : '' }} value="active"> Active</option>
              </select>
              @error('etat') <span class="text-danger">{{ $message }}</span> @enderror
            </div>

            <div class="col-lg-12">
              <br>
              <input type="hidden" name="banniere_id" value="{{$banniere->id}}">
              <button type="submit" class="btn btn-success btn-sm">ENREGISTRER</button>
            </div>
            </form>
          </div>


          <div class="row">
          </div>


        <br><br>
      </div>

    </div>
  </div>
</div>
<!--/ END row -->
@endsection

@section('js')
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <script src="{{ asset('assets/js/plugins/ckeditor/ckeditor.js')}}"></script>
    <script>
        CKEDITOR.replace('summary-ckeditor');
        CKEDITOR.replace('js-ckeditor1');
        CKEDITOR.replace('js-ckeditor2');
        CKEDITOR.replace('js-ckeditor3');
        CKEDITOR.replace('js-ckeditor4');
        CKEDITOR.replace('js-ckeditor5');
        CKEDITOR.replace('js-ckeditor6');
        CKEDITOR.replace('js-ckeditor7');
        CKEDITOR.replaceAll('about');
    </script>
    <script>
        jQuery(function () {
            Codebase.helpers(['flatpickr', 'datepicker', 'colorpicker', 'maxlength', 'select2', 'masked-inputs', 'rangeslider', 'tags-inputs', 'summernote', 'ckeditor', 'simplemde']);
        });

    </script>
@endsection
