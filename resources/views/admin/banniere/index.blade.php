@extends('layouts.layout')

@section('content')
<!-- START Template Container -->
<div class="container-fluid">
  <!-- Page Header -->
  <div class="page-header page-header-block">
    <div class="page-header-section">
      <h4 class="title semibold">Liste des Bannières</h4>
    </div>
    <div class="page-header-section">
      <!-- Toolbar -->
      <div class="toolbar clearfix">
        <div class="col-xs-8">
        </div>
        <div class="col-xs-4">
          <a class="btn btn-primary pull-right" href="{{route('param.banniere_add')}}"><i class="ico-settings mr5"></i>Ajouter</a>
        </div>
      </div>
      <!--/ Toolbar -->
    </div>
  </div>
  <!-- Page Header -->
  <!-- START row -->
  <div style="font-size:9px" class="row">
    <div class="col-md-12">
      <div class="panel panel-default" id="demo">
        <div class="panel-heading">
          <h3 class="panel-title"></h3>
        </div>
        <table class="table table-striped table-bordered" id="table-tools">
          <thead>
            <tr>
              <th width="3%">N°</th>
              <th width="10%">Image</th>
              <th>Titre</th>
              <th>Description</th>
              <th>Etat</th>
              <th width="25%">Action</th>
            </tr>
          </thead>
          <tbody>
            <?php $i=1; ?>
            @foreach($bannieres as $banniere)
            <tr>
              <td>{{$i++}}</td>
              <td align="center">
                <img src="{{asset($banniere->source)}}" class="img-fluid responsive" width="100%" alt="">
              </td>
              <td>{{$banniere->titre}}</td>
              <td>{{$banniere->description}}</td>
              <td>
                @if($banniere->etat != "")
                <a class="btn btn-xs btn-danger" href="#">
                  <i style="color:#fff" class="ico-check mr5"></i> {{$banniere->etat}}
                </a>
                @endif
              </td>
              <td>
                <a class="btn btn-xs btn-primary" href="{{route('param.banniere_edit', $banniere->id)}}">
                  <i style="color:#fff" class="ico-pencil mr5"></i> Editer
                </a>

                <a class="btn btn-xs btn-danger" href="" style="color:purple">
                  <form action="{{ route('param.delete_banniere') }}" method="post">
                    @csrf
                    <input type="hidden" name="id" value="{{$banniere->id}}" />
                    <button onclick="return confirm('Voulez-vous vraiment Supprimer ?')" class="btn btn-xs btn-primary" style="background-color: transparent; border: 0px;" type="submit" name="submit">
                      <i style="color:#000" class="fa fa-mobitrashle" aria-hidden="true"></i> Supprimer
                    </button>
                  </form>
                </a>
              </td>
            </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>
  <!--/ END row -->
</div>
@endsection
