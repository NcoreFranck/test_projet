
<div class="modal" id="myModal{{$media->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content" style="width:100%">
      <div class="modal-header">
        <p class="modal-title" id="myModalLabel">
          <label for="">{{$media->libelle}}: description</label>
        </p>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      </div>
        <div style="" class="modal-body">
          <div class="col-lg-12">
            {{$media->description}}
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Fermer</button>
        </div>
    </div>
  </div>
</div>
