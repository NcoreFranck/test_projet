<ul class="topmenu topmenu-responsive" data-toggle="menu">
  <li class="active open">
    <a href="{{route('dashboard')}}" data-target="#dashboard" data-toggle="submenu" data-parent=".topmenu">
      <span class="figure"><i class="ico-home4"></i></span>
      <span class="text">Accueil</span>
    </a>
  </li>

  <li class="heading">PARAMETRES</li>
  <li >
    <a href="{{ route('param.banniere') }}">
      <span class="figure"><i class="ico-settings"></i></span>
      <span class="text">Gestion des banieres</span>
    </a>
  </li>
    <li >
        <a href="{{ route('admin.mediatheque') }}">
            <span class="figure"><i class="ico-settings" aria-hidden="true"></i></span>
            <span class="text">Faire une publication</span>
        </a>
    </li>
    <li >
        <a href="{{ route('param.tags') }}">
            <span class="figure"><i class="ico-settings"></i></span>
            <span class="text">Gestion des tags</span>
        </a>
    </li>
  <li >
    <a href="{{ route('param.ville_commune') }}">
      <span class="figure"><i class="ico-settings"></i></span>
      <span class="text">Gestion des villes/communes</span>
    </a>
  </li>
    <li >
        <a href="{{ route('admin.membres_inscrits') }}">
            <span class="figure"><i class="ico-settings"></i></span>
            <span class="text">Membres inscrits</span>
        </a>
    </li>

  @hasrole('superadmin')
  <li class="heading">ADMINISTRATION</li>
  <li >
    <a href="{{ route('users') }}">
      <span class="figure"><i class="ico-group"></i></span>
      <span class="text">Users</span>
    </a>
  </li>
  <li >
    <a href="{{ route('roles') }}">
      <span class="figure"><i class="ico-screwdriver"></i></span>
      <span class="text">Rôles</span>
    </a>
  </li>
  @endhasrole
</ul>
