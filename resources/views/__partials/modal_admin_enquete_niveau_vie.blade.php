<!-- Button trigger modal -->

<button type="button"  class="btn btn-sm btn-danger" data-toggle="modal" data-target="#myModal">
  <i style="color:#000" class="fa fa-eye" aria-hidden="true"></i> Voir l'enquête du niveau de vie
</button>


<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" style="width:1200px" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">ENQUETE DU NIVEAU DE VIE</h4>
      </div>
      <br><br>
      <div class="col-lg-12">
        <table class="table">
          <thead>
            <th>N°</th>
            <th>QUESTIONS</th>
            <th>REPONSES</th>
            <th>NOTES</th>
          </thead>
          <tbody>
            <?php $i=1; ?>
            @foreach($demandeur_niveauvie_reponses as $demandeur_niveauvie_reponse)
            <tr>
              <td>{{$i++}}</td>
              <td>{{$demandeur_niveauvie_reponse->grille_reponse_get_gille_question->question ?? '' }}</td>
              <td>{{$demandeur_niveauvie_reponse->reponse ?? '' }}</td>
              <td>{{$demandeur_niveauvie_reponse->note ?? '' }}</td>
            </tr>
            @endforeach
            <tr>
              <td colspan="3" align="right"><b>TOTAL OBTENU</b> </td>
              <td colspan="1"><b>{{$notes}}</b> </td>
            </tr>
          </tbody>
        </table>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
      </div>
    </div>
  </div>
</div>
