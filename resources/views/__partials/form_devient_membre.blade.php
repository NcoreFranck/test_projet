<form method="post" action="{{route('devient_membre')}}"
      enctype="multipart/form-data"
      class="form-horizontal">
    {!! csrf_field() !!}

    <div class="row">
        <div align="center" class="col-lg-12">
            <label align="center" style="font-size: 25px"> <b>Avez-vous un projet que vous souhaitez réaliser ? <span style="color:red">*</span></b></label>
            <div>
                <label style="font-size: 25px" class="Radio" for="oui-projet">
                    <input class="Radio-Input" type="radio" id="oui-projet" name="onliste_porteur_projet" value="oui" />
                    Oui
                </label>

                <label style="font-size: 25px" class="Radio" for="non-projet">
                    <input class="Radio-Input" type="radio" id="non-projet" name="onliste_porteur_projet" value="non" />
                    Non
                </label>
            </div>
            @error('onliste_porteur_projet') <span class="text-danger">{{ $message }}</span> @enderror
        </div>

        <div class="col-lg-12">
            <label>Commune <span style="color:red">*</span></label>
            <select required class="form-control" name="ville_commune">
                <option value=""> .:: Sélectionner ::.</option>
                @foreach($ville_communes as $ville_commune)
                    <option
                        value="{{$ville_commune->id}}"> {{$ville_commune->libelle}}</option>
                @endforeach
            </select>
            @error('commune') <span
                class="text-danger">{{ $message }}</span> @enderror
        </div>

        <div class="col-lg-6">
            <label>Nom <span style="color:red">*</span></label>
            <input required class="form-control" type="text" value=""
                   name="nom" value="">
            @error('nom') <span
                class="text-danger">{{ $message }}</span> @enderror
        </div>

        <div class="col-lg-6">
            <label>Prénoms <span style="color:red">*</span></label>
            <input required class="form-control" type="text" value=""
                   name="prenoms" value="">
            @error('prenoms') <span
                class="text-danger">{{ $message }}</span> @enderror
        </div>

        <div class="col-lg-6">
            <label>Numéro de téléphone 1 <span style="color:red">*</span></label>
            <input required class="form-control" type="text" value=""
                   name="tel" value="">
            @error('tel') <span
                class="text-danger">{{ $message }}</span> @enderror
        </div>

        <div class="col-lg-6">
            <label>Numéro de téléphone 2</label>
            <input class="form-control" type="text" value=""
                   name="tel2" value="">
            @error('tel2') <span
                class="text-danger">{{ $message }}</span> @enderror
        </div>

        <div class="col-lg-6">
            <label>Email</label>
            <input class="form-control" type="email" value=""
                   name="email" value="">
            @error('email') <span
                class="text-danger">{{ $message }}</span> @enderror
        </div>

        <div class="col-lg-6">
            <label>Numéro Carte Nationale d'Identité</label>
            <input class="form-control" type="numero_cni"
                   name="numero_cni" value="">
            @error('numero_cni') <span
                class="text-danger">{{ $message }}</span> @enderror
        </div>

        <div class="col-lg-6">
            <label>Numéro Carte d'Électeur</label>
            <input class="form-control" type="numero_ce"
                   name="numero_ce" value="">
            @error('numero_ce') <span
                class="text-danger">{{ $message }}</span> @enderror
        </div>

        <div style="padding-bottom: 10px" class="col-lg-6">
            <label>Votre date de naissance <span style="color:red">*</span></label>
            <input class="form-control" type="date"
                   name="date_naiss" value="">
            @error('date_naiss') <span
                class="text-danger">{{ $message }}</span> @enderror
        </div>

        <div class="col-lg-6">
            <label>Genre <span style="color:red">*</span></label>
            <select required class="form-control" name="sexe">
                <option value=""> .:: Sélectionner ::.</option>
                <option value="MASCULIN"> MASCULIN</option>
                <option value="FEMININ"> FEMININ</option>
            </select>
            @error('sexe') <span
                class="text-danger">{{ $message }}</span> @enderror
        </div>

        <div class="col-lg-6">
            <label>Activités <span style="color:red">*</span></label>
            <select required class="form-control" name="activites">
                <option value=""> .:: Sélectionner ::.</option>
                <option value="ÉLEVE"> ÉLÈVE</option>
                <option value="ÉTUDIANT"> ÉTUDIANT(E)</option>
                <option value="COMMERCANT"> COMMERCANT(E)</option>
                <option value="FONCTIONNAIRE"> FONCTIONNAIRE</option>
                <option value="CONTRACTUEL"> CONTRACTUEL(LE)</option>
                <option value="RECHERCHE D'EMPLOI"> RECHERCHE D'EMPLOI</option>
            </select>
            @error('activites') <span
                class="text-danger">{{ $message }}</span> @enderror
        </div>

        <div class="col-lg-12"><br>
            <label>Êtes-vous inscrit(e) sur une liste électorale à Cocody ? <span style="color:red">*</span></label>
            <div>
                <label class="Radio" for="oui">
                    <input class="Radio-Input" type="radio" id="oui" name="onliste_elect" value="oui" />
                    Oui
                </label>

                <label class="Radio" for="non">
                    <input class="Radio-Input" type="radio" id="non" name="onliste_elect" value="non" />
                    Non
                </label>
            </div>
            @error('numero_cni') <span class="text-danger">{{ $message }}</span> @enderror
        </div>

        <input type="hidden" name="_publication_note" value="">
        @if(isset($mediatheque))
            <input type="hidden" name="_publicationId" value="{{$mediatheque->id}}">
        @endif

        <div class="col-lg-12 text-center">
            <button style="font-size: 14px" type="submit" class="cta-btn align-middle"> <b>JE REJOINS LA TEAM ERIC TABA</b>
            </button>
        </div>
    </div>

</form>
