<section id="services" class="services">
  <div class="container">

    <div class="row">
      <div class="col-lg-4 col-md-6">
        <a href="{{ url('demande_prise_charge') }}">
          <div class="icon-box" data-aos="fade-up">
            <div class="icon"><i class="fa fa-briefcase"></i></div>
            <h4 class="title"><a href="">Démande de prise en charge</a></h4>
            <p class="description">Vous avez des difficultés pour scolariser votre enfant ? faites une demande de prise en charge</p>
          </div>
        </a>
      </div>
      <div class="col-lg-4 col-md-6">
        <a href="{{ url('faire_don') }}">
          <div class="icon-box" data-aos="fade-up" data-aos-delay="100">
            <div class="icon"><i class="fa fa-btc"></i></div>
            <h4 class="title"><a href="">Faire un don</a></h4>
            <p class="description">Participez à l'éducation de la gente feminine en effectuant un don</p>
          </div>
        </a>
      </div>
      <div class="col-lg-4 col-md-6">
        <a href="{{ url('suivie_encadrement') }}">
          <div class="icon-box" data-aos="fade-up" data-aos-delay="200">
            <div class="icon"><i class="fa fa-bar-chart"></i></div>
            <h4 class="title"><a href="">Suivi et encadrement</a></h4>
            <p class="description">La clef du succès scolaire se trouve dans un meilleur suivi et encadrement de nos BABYBLOOM...</p>
          </div>
        </a>
      </div>
    </div>

  </div>
</section>
