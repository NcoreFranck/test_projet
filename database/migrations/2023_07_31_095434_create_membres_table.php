<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMembresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('membres', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->integer('ville_commune')->nullable();
            $table->string('nom', 20)->nullable();
            $table->string('prenoms', 100)->nullable();
            $table->string('tel', 20)->nullable();
            $table->string('tel2', 20)->nullable();
            $table->string('add_mac', 190)->nullable();

            $table->string('onliste_elect', 10)->nullable();
            $table->string('activites', 25)->nullable();
            $table->string('numero_cni', 50)->nullable();
            $table->date('date_naiss')->nullable();

            $table->string('onliste_porteur_projet', 50)->nullable();
            $table->string('numero_ce', 50)->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('membres');
    }
}
