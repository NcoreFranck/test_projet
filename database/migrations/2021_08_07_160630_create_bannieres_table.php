<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBannieresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bannieres', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();
            $table->text('source')->nullable();
            $table->string('titre', 191)->nullable();
            $table->text('description')->nullable();
            $table->text('autre_info')->nullable();
            $table->string('etat', 20)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bannieres');
    }
}
