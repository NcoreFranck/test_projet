<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // SUPERADMIN
        $superadmin = Role::create(['name' => 'superadmin']);
        $admin = Role::create(['name' => 'admin']);

        $permission = Permission::create(['name' => 'manage_users']);
        $permission->assignRole($superadmin);

        // Assigning roles to a user

        $user = User::whereEmail('kouakou.ncore@uvci.edu.ci')->first();
        $user->assignRole($superadmin, $admin);

        $user = User::whereEmail('junoserge@yahoo.fr')->first();
        $user->assignRole($superadmin, $admin);
    }
}
