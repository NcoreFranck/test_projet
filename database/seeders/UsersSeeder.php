<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Webpatser\Uuid\Uuid;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */

    public static function generateUuid()
    {
        return Uuid::generate();
    }
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'NCORE Franck',
            'email' => 'kouakou.ncore@uvci.edu.ci',
            'password' => Hash::make('nfpassword'),
            'uuid'=>self::generateUuid(),
        ]);

        DB::table('users')->insert([
            'name' => 'JUNO',
            'email' => 'junoserge@yahoo.fr',
            'password' => Hash::make('junoserge@yahoo.fr'),
            'uuid'=>self::generateUuid(),
        ]);

    }
}
