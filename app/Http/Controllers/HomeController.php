<?php

namespace App\Http\Controllers;

use App\Models\Banniere;
use App\Models\Media;
use App\Models\Membre;
use App\Models\Partenaire;
use App\Models\PublicationRate;
use App\Models\VilleCommune;
use Illuminate\Http\Request;

use DateTime;
use Exception;
use Illuminate\Support\Facades\DB;
use Auth;
use Carbon\Carbon;
use Illuminate\Support\Str;
use Session;

use App\Countrie;
use PDF;
use Illuminate\Support\Facades\Hash;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $data['bannieres'] = Banniere::orderBy('created_at', 'asc')->get();
        $data['partenaires'] = Partenaire::orderBy('created_at', 'asc')->get();
        $data['banniere_active'] = Banniere::where('etat', 'active')->first();
        if(is_null($data['banniere_active'])){
            $data['banniere_active'] = new Banniere();
        }
        $data['ville_communes'] = VilleCommune::orderBy('created_at', 'asc')->get();
        return view('index')->with($data);
    }

    public function partenaire_info($id)
    {
        $data['partenaire'] = Partenaire::find($id);
        return view('partenaire_info')->with($data);
    }

    public function front_mediatheque()
    {
        $data['mediatheques'] = Media::orderBy('created_at', 'asc')->get();
        return view('front.media.index')->with($data);
    }

    public function mediatheque_view($id)
    {
        $add_mac = substr(exec('getmac'), 0, 17);
        $data['membre'] = Membre::where('add_mac', $add_mac)->first();
        $data['publication_rate'] = null;
        if(!is_null($data['membre'])){
            $data['publication_rate'] = PublicationRate::where('membre_id', $data['membre']->id)
                ->where('publication_id', $id)->first();
        }
        $data['ville_communes'] = VilleCommune::orderBy('created_at', 'asc')->get();
        $data['mediatheque'] = Media::find($id);
        if (!is_null($data['mediatheque'])) {
            return view('front.media.media_view')->with($data);
        }
        session()->flash('message_error', 'Désolé ! Aucun media disponible dans cette rubrique.');
        return redirect()->back();
    }

    public function devient_membre(Request $request)
    {
        $date = new DateTime(null);
        $this->validate(
            $request,
            [
                'ville_commune' => 'required',
                'nom' => 'required',
                'prenoms' => 'required',
                'tel' => 'required',

                'onliste_elect' => 'required',
                'activites' => 'required',
                'date_naiss' => 'required',

                'onliste_porteur_projet' => 'required',
                'sexe' => 'required',
            ]
        );
        $add_mac = substr(exec('getmac'), 0, 17);
        $attribute = [
            'ville_commune' => trim($request->ville_commune),
            'nom' => trim($request->nom),
            'prenoms' => $request->prenoms,
            'tel' => $request->tel,
            'tel2' => $request->tel2,
            'add_mac' => $add_mac,

            'onliste_elect' => trim($request->onliste_elect),
            'activites' => $request->activites,
            'date_naiss' => $request->date_naiss,
            'numero_cni' => $request->numero_cni,

            'onliste_porteur_projet' => $request->onliste_porteur_projet,
            'numero_ce' => $request->numero_ce,
            'sexe' => $request->sexe,
            'email' => $request->email,
        ];

        DB::beginTransaction();
        try {
            if($request->_membre_id){
                $_membre = Membre::find($request->_membre_id);
                $rs = $_membre->update($attribute);
            }else {
                $rs = Membre::create($attribute);
            }

            if($request->_publication_note){
                $attribute = [
                    'note' => $request->_publication_note,
                    'publication_id' => $request->_publicationId,
                    'membre_id' => $rs->id,
                ];
                $pub = PublicationRate::create($attribute);
            }

            DB::commit();
            flash("Félicitation, bienvenue dans la famille des champions.")->success();
            return redirect()->back();
        } catch (Exception $exception) {
            DB::rollBack();
            flash($exception->getMessage())->error();
            return redirect()->back();
        }
    }

    public function save_publication_rate(Request $request)
    {
        $add_mac = substr(exec('getmac'), 0, 17);
        $request->validate([
            '_note' => 'required',
            '_publication' => 'required',
        ]);
        $membre = Membre::where('add_mac', $add_mac)->first();
        $attribute = [
            'note' => $request->_note,
            'publication_id' => $request->_publication,
            'membre_id' => $membre->id,
        ];
        $rs = PublicationRate::create($attribute);
        return response()->json([
            'success' => 'Successfully',
            'response' => $rs,
        ]);
    }

}
