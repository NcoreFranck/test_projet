<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use DateTime;
use Exception;
use Illuminate\Support\Facades\DB;
use Auth;
use Carbon\Carbon;
use Illuminate\Support\Str;
use Session;

use App\me;
use App\Models\Tag;
use App\Models\Banniere;
use App\Models\VilleCommune;
use App\Models\User;
use App\Models\Partenaire;

class ParametreController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $data['bannieres'] = Banniere::orderBy('created_at', 'asc')->get();
        return view('admin.banniere.index')->with($data);
    }

    public function banniere_add($id = NULL)
    {
        if ($id) {
            $data['banniere'] = Banniere::find($id);
            $data['add_update'] = route('param.banniere_update');
        } else {
            $data['banniere'] = new Banniere();
            $data['add_update'] = route('param.banniere_save');
        }
        return view('admin.banniere.banniere_add')->with($data);
    }

    public function banniere_save(Request $request)
    {
        $date = new DateTime(null);
        $this->validate(
            $request,
            [
                'titre' => 'required',
                'description' => 'required',
                'autre_info' => 'required',
            ]
        );

        $attribute = [
            'titre' => trim($request->titre),
            'description' => trim($request->description),
            'etat' => $request->etat,
            'autre_info' => $request->autre_info,
        ];

        if ($request->source) {
            $piece_name = '/sources/requet/banniere/banniere_' . $date->format('dmYhis') . '.' . $request->source->getClientOriginalExtension();
            $request->source->move('sources/requet/banniere/', $piece_name);
            $attribute['source'] = $piece_name;
        }

        DB::beginTransaction();
        try {
            if ($request->etat != '') {
                $old_bannieres = Banniere::Where('etat', $request->etat)->get();
                if (!is_null($old_bannieres)) {
                    foreach ($old_bannieres as $old_banniere) {
                        $old_banniere->update(['etat' => NULL]);
                    }
                }
            }

            if ($request->banniere_id) {
                $demand = Banniere::find($request->banniere_id);
                $demand->update($attribute);
            } else {
                $demand = Banniere::create($attribute);
            }
            DB::commit();
            flash('Operation effectué avec succès')->success();
            return redirect()->route('param.banniere');
        } catch (Exception $exception) {
            DB::rollBack();
            flash($exception->getMessage())->error();
            return redirect()->back();
        }
    }


    public function partenaire()
    {
        $data['partenaires'] = Partenaire::orderBy('created_at', 'asc')->get();
        return view('admin.partenaire.index')->with($data);
    }

    public function partenaire_add($id = NULL)
    {
        if ($id) {
            $data['partenaire'] = Partenaire::find($id);
            $data['add_update'] = route('param.partenaire_update');
        } else {
            $data['partenaire'] = new Partenaire();
            $data['add_update'] = route('param.partenaire_save');
        }
        return view('admin.partenaire.partenaire_add')->with($data);
    }

    public function partenaire_save(Request $request)
    {
        $date = new DateTime(null);
        $this->validate(
            $request,
            [
                'description' => 'required',
                'entreprise_nom' => 'required',
            ]
        );

        $attribute = [
            'description' => trim($request->description),
            'entreprise_nom' => trim($request->entreprise_nom),
            'site_internet' => trim($request->site_internet),
            'lien_video' => trim($request->lien_video),
        ];

        if ($request->partenaire_id == '') {
            $this->validate(
                $request,
                [
                    'logo' => 'required',
                ]
            );
        }

        if ($request->logo) {
            $time1 = date('YmdHis') . uniqid();
            $attribute['logo'] = 'storage/requet/partenaire/' . $time1 . '.' . $request->logo->getClientOriginalExtension();
            $request->logo->storeAs('public/requet/partenaire', $time1 . '.' . $request->logo->getClientOriginalExtension());
        }

        DB::beginTransaction();
        try {
            if ($request->partenaire_id) {
                $_partenaire = Partenaire::find($request->partenaire_id);
                $_partenaire->update($attribute);
            } else {
                $_partenaire = Partenaire::create($attribute);
            }
            DB::commit();
            flash('Operation effectué avec succès')->success();
            return redirect()->route('param.partenaire');
        } catch (Exception $exception) {
            DB::rollBack();
            flash($exception->getMessage())->error();
            return redirect()->back();
        }
    }

    public function delete_partenaire(Request $request)
    {
        $row = Partenaire::find($request->id);
        if (!is_null($row)) {
            $row->delete();
            flash("Suppression effectuée")->success();
            return redirect()->back();
        } else {
            flash("Oups! une erreur s'est produite.")->error();
        }
        return redirect()->back();
    }

    public function delete_banniere(Request $request)
    {
        $row = Banniere::find($request->id);
        if (!is_null($row)) {
            $row->delete();
            flash("Suppression effectuée")->success();
            return redirect()->back();
        } else {
            flash("Oups! une erreur s'est produite.")->error();
        }
        return redirect()->back();
    }


    public function ville_commune()
    {
        $data['ville_communes'] = VilleCommune::orderBy('libelle', 'asc')->get();
        return view('admin.ville_commune.index')->with($data);
    }

    public function ville_commune_add($id = NULL)
    {
        if ($id) {
            $data['ville_commune'] = VilleCommune::find($id);
            $data['add_update'] = route('param.ville_commune_update');
        } else {
            $data['ville_commune'] = new VilleCommune();
            $data['add_update'] = route('param.ville_commune_save');
        }
        return view('admin.ville_commune.ville_commune_add')->with($data);
    }

    public function ville_commune_save(Request $request)
    {
        $date = new DateTime(null);
        $this->validate(
            $request,
            [
                'libelle' => 'required',
            ]
        );
        $attribute = [
            'libelle' => trim(mb_strtoupper($request->libelle)),
        ];
        DB::beginTransaction();
        try {
            if ($request->ville_commune_id) {
                $vill_comm = VilleCommune::find($request->ville_commune_id);
                $vill_comm->update($attribute);
            } else {
                $vill_comm = VilleCommune::create($attribute);
            }
            DB::commit();
            flash('Operation effectué avec succès')->success();
            return redirect()->route('param.ville_commune');
        } catch (Exception $exception) {
            DB::rollBack();
            flash($exception->getMessage())->error();
            return redirect()->back();
        }
    }


    public function tags()
    {
        $data['tags'] = Tag::orderBy('libelle', 'asc')->get();
        return view('admin.tags.index')->with($data);
    }

    public function tags_add($id = NULL)
    {
        if ($id) {
            $data['tag'] = Tag::find($id);
            $data['add_update'] = route('param.tags_update');
        } else {
            $data['tag'] = new Tag();
            $data['add_update'] = route('param.tags_save');
        }
        return view('admin.tags.tags_add')->with($data);
    }

    public function tags_save(Request $request)
    {
        $date = new DateTime(null);
        $this->validate(
            $request,
            [
                'libelle' => 'required',
            ]
        );
        $attribute = [
            'libelle' => trim(mb_strtoupper($request->libelle)),
        ];
        DB::beginTransaction();
        try {
            if ($request->tag_id) {
                $_Tag = Tag::find($request->tag_id);
                $_Tag->update($attribute);
            } else {
                $_Tag = Tag::create($attribute);
            }
            DB::commit();
            flash('Operation effectué avec succès')->success();
            return redirect()->route('param.tags');
        } catch (Exception $exception) {
            DB::rollBack();
            flash($exception->getMessage())->error();
            return redirect()->back();
        }
    }

    public function delete_tag(Request $request)
    {
        $row = Tag::find($request->id);
        if (!is_null($row)) {
            $row->delete();
            flash("Suppression effectuée")->success();
            return redirect()->back();
        } else {
            flash("Oups! une erreur s'est produite.")->error();
        }
        return redirect()->back();
    }
}
