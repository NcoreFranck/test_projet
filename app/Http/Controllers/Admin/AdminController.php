<?php

namespace App\Http\Controllers\Admin;

use App\Exports\ExportMembre;
use App\Http\Controllers\Controller;
use App\Models\Banniere;
use App\Models\Membre;
use App\Models\Partenaire;
use App\Models\VilleCommune;
use Barryvdh\DomPDF\Facade\Pdf;
use Illuminate\Http\Request;

use DateTime;
use Exception;
use Illuminate\Support\Facades\DB;
use Auth;
use Carbon\Carbon;
use Illuminate\Support\Str;
use Maatwebsite\Excel\Facades\Excel;
use Session;

class AdminController extends Controller
{

  public function __construct()
  {
    $this->middleware('auth');
  }

  public function index()
  {
    return view('admin.index');
  }

    public function membres_inscrits()
    {
        $data['membres_inscrits'] = Membre::orderBy('nom', 'asc')->orderBy('prenoms', 'asc')->get();
        return view('admin.membres.index')->with($data);
    }

    public function membres_inscrit_edit($id)
    {
        $data['membre_inscrit'] = Membre::find($id);
        $data['bannieres'] = Banniere::orderBy('created_at', 'asc')->get();
        $data['partenaires'] = Partenaire::orderBy('created_at', 'asc')->get();
        $data['ville_communes'] = VilleCommune::orderBy('created_at', 'asc')->get();
        return view('admin.membres.membre_add')->with($data);
    }

    public function membres_inscrits_print($uri)
    {
        $data['membres_inscrits'] = Membre::orderBy('nom', 'asc')->orderBy('prenoms', 'asc')->get();
        if($uri == 'pdf'){
            setlocale(LC_TIME, 'fr_FR.UTF8', 'fr.UTF8', 'fr_FR.UTF-8', 'fr.UTF-8');
            $data['date_impression'] = strftime("%d %B %Y", strtotime(now()));
            $pdf = PDF::loadView("admin.print.membres_inscrits", $data);
            return $pdf->stream('membres_inscrits.pdf');
        }
        if($uri == 'excel'){
            return Excel::download(new ExportMembre($data), 'membres.xlsx');
        }
    }

    public function membres_inscrit_delete(Request $request)
    {
        $row = Membre::find($request->id);
        if (!is_null($row)) {
            $row->delete();
            flash("Suppression effectuée")->success();
            return redirect()->back();
        } else {
            flash("Oups! une erreur s'est produite.")->error();
        }
        return redirect()->back();
    }

}
