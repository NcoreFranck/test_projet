<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Tag;
use Illuminate\Http\Request;
use DateTime;
use Exception;
use Illuminate\Support\Facades\DB;
use Auth;
use Carbon\Carbon;
use Illuminate\Support\Str;
use Session;

use App\Countrie;
use App\Models\User;
use App\Models\Media;

class MediathequeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $data['medias'] = Media::orderBy('created_at', 'asc')->get();
        return view('admin.media.index')->with($data);
    }

    public function mediatheque_add($id = NULL)
    {
        if ($id) {
            $data['media'] = Media::find($id);
            $data['add_update'] = route('admin.mediatheque_update');
            $data['mes_tags'] = $data['media']->media_get_tags->pluck('id')->toArray();
        } else {
            $data['media'] = new Media();
            $data['add_update'] = route('admin.mediatheque_save');
            $data['mes_tags'] = [];
        }
        $data['tags'] = Tag::orderBy('libelle', 'asc')->get();
        return view('admin.media.mediatheque_add')->with($data);
    }

    public function mediatheque_save(Request $request)
    {
        $date = new DateTime(null);
        $this->validate(
            $request,
            [
                'libelle' => 'required',
                'description' => 'required',
            ]
        );

        $attribute = [
            'libelle' => trim($request->libelle),
            'description' => trim($request->description),
        ];

        if ($request->source) {
            $piece_name = '/sources/requet/media/media_' . $date->format('dmYhis') . '.' . $request->source->getClientOriginalExtension();
            $request->source->move('sources/requet/media/', $piece_name);
            $attribute['source'] = $piece_name;
        }

        DB::beginTransaction();
        try {
            if ($request->media_id) {
                $media = Media::find($request->media_id);
                $media->update($attribute);
                $media->media_get_tags()->sync($request->tags);
            } else {
                $media = Media::create($attribute);
                $media->media_get_tags()->attach($request->tags);
            }
            DB::commit();
            flash('Operation effectué avec succès')->success();
            return redirect()->route('admin.mediatheque');
        } catch (Exception $exception) {
            DB::rollBack();
            flash($exception->getMessage())->error();
            return redirect()->back();
        }
    }

    public function piece_delete(Request $request)
    {
        $row = Piece::find($request->id);
        if (!is_null($row)) {
            $row->delete();
            flash("Suppression effectuée")->success();
            return redirect()->back();
        } else {
            flash("Oups! une erreur s'est produite.")->error();
        }
        return redirect()->back();
    }

    public function mediatheque_delete(Request $request)
    {
        $row = Media::find($request->id);
        if (!is_null($row)) {
            $row_childs = $row->media_get_pieces;
            if (!is_null($row_childs)) {
                foreach ($row_childs as $row_child) {
                    $row_child->delete();
                }
            }
            $row->delete();
            flash("Suppression effectuée")->success();
            return redirect()->back();
        } else {
            flash("Oups! une erreur s'est produite.")->error();
        }
        return redirect()->back();
    }

}
