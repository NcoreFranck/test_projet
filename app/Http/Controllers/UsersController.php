<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use DB;
use Illuminate\Support\Collection;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Webpatser\Uuid\Uuid;

class UsersController extends Controller
{
  public function __construct()
  {
    $this->middleware('auth');
    $this->middleware('permission:manage_users|manage_admin|manage_services|manage_correspondants|manage_courriers|manage_arrivees|manage_arrivees|manage_departs|manage_registre|manage_imputation|manage_traitement', ['only' => ['index','dircab_index','ajouter','store_arrivee','transmettre_arrivee','lire_arrivee','delete_arrivee','generateUuid','index','add_user','save_user','delete_user','role','create_role','save_role','delete_role']]);
    // $this->middleware('permission:manage_users', ['only' => ['index','','']]);
    // $this->middleware('permission:manage_admin', ['only' => ['index','','']]);
    // $this->middleware('permission:manage_services', ['only' => ['index','','']]);
    // $this->middleware('permission:manage_correspondants', ['only' => ['index','','']]);
    // $this->middleware('permission:manage_courriers', ['only' => ['index','','']]);
    // $this->middleware('permission:manage_arrivees', ['only' => ['index','','']]);
    // $this->middleware('permission:manage_departs', ['only' => ['index','','']]);
    // $this->middleware('permission:manage_registre', ['only' => ['index','','']]);
    // $this->middleware('permission:manage_imputation', ['only' => ['index','','']]);
    // $this->middleware('permission:manage_traitement', ['only' => ['index','','']]);
  }
  public static function generateUuid()
  {
    return Uuid::generate();
  }

  public function index()
  {
    $data['users'] = User::orderBy('name','ASC')->get();
    return view('users.users')->with($data);
  }
  public function add_user($user_uuid = null)
  {

    $data['roles'] = Role::all()->pluck('name', 'id');
    if($user_uuid){
      //dd('rentrer');
      $data['user'] = User::where('uuid', $user_uuid)->first();
      $data['mes_roles'] = $data['user']->roles->pluck('name')->toArray();
      $data['route_action'] = route('update_user', $user_uuid);
    }else{
      $data['route_action'] = route('save_user');
      $data['user'] = new User();
      $data['mes_roles'] =[];
    }
    //dd('sortir');
    return view('users.form_user')->with($data);
  }

  public function save_user(Request $request, $user_uuid = null)
  {
    //dd($request->all());

    if($user_uuid)
    {
      $user = User::where('uuid', $user_uuid)->first();
      $validation = Validator::make($request->all(), [
        'name' => 'required',
        'email' => 'required|email|unique:users,email,' . $user->id,
        'roles' => 'required',
      ]);
      $attribute = [
        'name' => $request->name,
        'email' => $request->email,
      ];
      if($request->password !='')
      {
        $validator = Validator::make($request->all(), [
          'password' => 'required|confirmed|min:6',
          'password_confirmation' => 'required|min:6',
          ])->validate();
          $attribute = [
            'password' => Hash::make($request->password),
          ];
        }
        $u = $user->update($attribute);
        // remove old roles
        if ($user->roles) {
          foreach ($user->roles as $role) {
            $user->removeRole($role);
          }
        }
        // add new roles
        $roles = $request->roles;
        foreach ($roles as $role) {
          $user->assignRole($role);
        }

        if ($u) {
          flash('Opération terminée avec succès')->success();
          return redirect()->route('users');
        }
      }else{
        $validator = Validator::make($request->all(), [
          'name' => 'required',
          'email' => 'required|email|unique:users,email',
          'roles' => 'required',
          'password' => 'required|confirmed|min:6',
          'password_confirmation' => 'required|min:6',
          ])->validate();

          $attribute = [
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
            'uuid'=>self::generateUuid(),
          ];
          //dd($attribute);
          $user = User::create($attribute);
          if ($user) {
            $roles = $request->roles;
            foreach ($roles as $role) {
              $user->assignRole($role);
            }
            flash("Opération terminée avec succès")->success();
            return redirect()->route('users');
          }
        }
        return redirect()->route('users');
      }

      public function delete_user(Request $request)
      {
        $user = User::findOrFail($request->user_uuid);
        if ($user) {
          $user->delete();
          flash("Opération terminée avec succès")->success();
          return redirect()->route('users');
        }
      }

      public function role()
      {
        $data['roles'] = Role::orderBy('id','DESC')->get();
        return view('users.roles')->with($data);
      }
      public function create_role($role_id = null)
      {
        $data['permissions'] = Permission::all();
        if($role_id){
          //dd('rentrer modif');
          $data['role'] = Role::find($role_id);
          $data['mes_permissions'] = DB::table("role_has_permissions")->where("role_has_permissions.role_id",$role_id)->pluck('permission_id')->toArray();
          $data['route_action'] = route('update_role', $role_id);
        }else{
          //dd('ajout');
          $data['route_action'] = route('save_role');
          $data['role'] = new Role();
          $data['mes_permissions'] = [];
        }
        //dd('sortir');
        return view('users.form_role')->with($data);
      }
      public function save_role(Request $request, $role_id = null)
      {
        //dd($request->all());
        if ($role_id) {
          $this->validate($request, [
            'name' => 'required',
            'permission' => 'required',
          ]);
          $role = Role::find($role_id);
          $role->name = $request->input('name');
          $role->save();
          $role->syncPermissions($request->input('permission'));
          flash("Opération terminée avec succès")->success();
          return redirect()->route('roles');
        }else{
          $this->validate($request, [
            'name' => 'required|unique:roles,name',
            'permission' => 'required',
          ]);
          $role = Role::create(['name' => $request->input('name')]);
          $role->syncPermissions($request->input('permission'));
          flash("Opération terminée avec succès")->success();
          return redirect()->route('roles');
        }

      }
      public function delete_role(Request $request)
      {
        $role = Role::findOrFail($request->id);
        if ($role) {
          $role->delete();
          flash("Opération terminée avec succès")->success();
          return redirect()->route('roles');
        }
      }

      public function profile()
      {
        return view('auth.profile');
      }
      public function update_profile(Request $request)
      {
        //dd($request->all());
        $user = User::where('uuid', Auth::user()->uuid)->first();

        $validator = Validator::make($request->all(), [
          'name' => 'required',
          'email' => 'required|email|unique:users,email,'.$user->id,
          ])->validate();
          $attribute = [
            'name' => strtoupper(trim($request->name)),
            'email' => strtolower(trim($request->email)),
          ];

          if ($request->password !=NULL) {
            $validator = Validator::make($request->all(), [
              'password_actuel' => 'required|min:8',
              'password' => 'same:password_confirm',
              ])->validate();
              //nfpassword
              if( ! Hash::check($request->input('password_actuel') , $user->password  ) )
              {
                flash("Le Mot de passe est incorrecte")->error();
                return redirect()->route('profile');
              }else {
                $user['password'] = Hash::make($request->password);
              }
            }

            DB::beginTransaction();
            try {
              $us = $user->update($attribute);
              DB::commit();
              flash("Opération terminée avec succès")->success();
              return redirect()->back();
            } catch (Exception $exception) {
              DB::rollBack();
              flash($exception->getMessage())->error();
              return redirect()->route('profile');
            }
          }

        }
