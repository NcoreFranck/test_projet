<?php

namespace App\Exports;

use App\Models\Membre;
use Maatwebsite\Excel\Concerns\FromCollection;

class ExportMembre implements FromCollection
{
    /**
    * @return \Illuminate\Support\Collection
    */

    protected $data;

    function __construct($data)
    {
        $this->data = $data;
    }

    public function collection()
    {
        $membres_inscrits = $this->data['membres_inscrits'];

        $result_array[] = array(
            'N°',
            'Nom',
            'Prénoms',
            'Email',
            'Sexe',
            'Numéro de téléphone 1',
            'Numéro de téléphone 2',
            'Commune',
            'Numéro CNI',
            'Numéro CE',
            "Présence sur la liste d'électeur",
            "Porteur de projets",
            "Date d'inscription",
            "Date de naissance",
            "Activité"
        );

        $i = 1;
        foreach ($membres_inscrits as $result) {
            $result_array[$i] = array(

                'N°' => $i,
                'Nom' => $result->nom,
                'Prénoms' => $result->prenoms,
                'Email' => $result->emails,
                'Sexe' => $result->sexe,
                'Numéro de téléphone 1' => $result->tel,
                'Numéro de téléphone 2' => $result->tel2,
                'Commune' => $result->membre_get_commune_ville->libelle,
                'Numéro CNI' => $result->numero_cni,
                'Numéro CE' => $result->numero_ce,
                "Présence sur la liste d'électeur" => $result->onliste_elect,
                "Porteur de projets" => $result->onliste_porteur_projet,
                "Date d'inscription" => $result->created_at,
                "Date de naissance" => $result->date_naiss,
                "Activité" => $result->activites,

            );
            $i++;
        }
        return collect($result_array);
    }
}
