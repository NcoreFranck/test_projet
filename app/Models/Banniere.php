<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Banniere extends Model
{
    protected $table = 'bannieres';
    public $timestamps = true;
    protected $guarded = [
        'id',
    ];
}
