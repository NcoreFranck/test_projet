<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MediaTag extends Model
{
    public $timestamps = true;
    protected $table = 'media_tags';
    protected $guarded = ['id'];

}
