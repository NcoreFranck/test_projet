<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Membre extends Model
{
    protected $table = 'membres';
    public $timestamps = true;
    protected $guarded = [
        'id',
    ];

    public function membre_get_commune_ville()
    {
        return $this->belongsTo('App\Models\VilleCommune', 'ville_commune');
    }

}
