<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PublicationRate extends Model
{
    protected $table = 'publication_rates';
    public $timestamps = true;
    protected $guarded = [
        'id',
    ];
}
