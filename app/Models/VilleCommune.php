<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class VilleCommune extends Model
{
    protected $table = 'ville_communes';
    public $timestamps = true;
    protected $guarded = [
        'id',
    ];
}
