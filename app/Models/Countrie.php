<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Countrie extends Model
{
  protected $table = 'countries';
  public $timestamps = true;
  protected $guarded = [
  'id',
  ];
}
