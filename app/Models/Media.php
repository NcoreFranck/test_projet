<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Media extends Model
{
    protected $table = 'medias';
    public $timestamps = true;
    protected $guarded = [
        'id',
    ];

    public function media_get_tags()
    {
        return $this->belongsToMany('App\Models\Tag', 'media_tags');
    }
}
