<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

require __DIR__.'/auth.php';




Route::get('/', 'App\Http\Controllers\HomeController@index')->name('accueil');
Route::get('/partenaire_info/{id}', 'App\Http\Controllers\HomeController@partenaire_info')->name('partenaire_info');

Route::group(['prefix' => 'front'], function () {
    Route::get('front_mediatheque', 'App\Http\Controllers\HomeController@front_mediatheque')->name('front_mediatheque');
    Route::get('mediatheque_view/{id}', 'App\Http\Controllers\HomeController@mediatheque_view')->name('mediatheque_view');
    Route::post('/devient_membre', 'App\Http\Controllers\HomeController@devient_membre')->name('devient_membre');
    Route::post('/save_publication_rate', 'App\Http\Controllers\HomeController@save_publication_rate')->name('save_publication_rate');
});

Route::get('/profile', 'App\Http\Controllers\UsersController@profile')->name('profile');
Route::post('/update_profile', 'App\Http\Controllers\UsersController@update_profile')->name('update_profile');

Route::get('/users', 'App\Http\Controllers\UsersController@index')->name('users');
Route::get('/add_user', 'App\Http\Controllers\UsersController@add_user')->name('add_user');
Route::post('/save_user', 'App\Http\Controllers\UsersController@save_user')->name('save_user');
Route::get('/edit_user/{user_uuid}', 'App\Http\Controllers\UsersController@add_user')->name('edit_user');
Route::post('/update_user/{user_uuid}', 'App\Http\Controllers\UsersController@save_user')->name('update_user');
Route::post('/delete_user', 'App\Http\Controllers\UsersController@delete_user')->name('delete_user');

Route::get('/roles', 'App\Http\Controllers\UsersController@role')->name('roles');
Route::get('/create_role', 'App\Http\Controllers\UsersController@create_role')->name('create_role');
Route::post('/save_role', 'App\Http\Controllers\UsersController@save_role')->name('save_role');
Route::get('/edit_role/{role_id}', 'App\Http\Controllers\UsersController@create_role')->name('edit_role');
Route::post('/update_role/{role_id}', 'App\Http\Controllers\UsersController@save_role')->name('update_role');
Route::post('/delete_role', 'App\Http\Controllers\UsersController@delete_role')->name('delete_role');


Route::get('home', 'App\Http\Controllers\UsersController@index')->name('dashboard');
Route::get('membres_inscrits', 'App\Http\Controllers\Admin\AdminController@membres_inscrits')->name('admin.membres_inscrits');
Route::get('membres_inscrit_edit/{id}', 'App\Http\Controllers\Admin\AdminController@membres_inscrit_edit')->name('admin.membres_inscrit_edit');
Route::post('membres_inscrit_delete', 'App\Http\Controllers\Admin\AdminController@membres_inscrit_delete')->name('admin.membres_inscrit_delete');
Route::get('membres_inscrits_print/{uri}', 'App\Http\Controllers\Admin\AdminController@membres_inscrits_print')->name('admin.membres_inscrits_print');
Route::post('membres_inscrit_save', 'App\Http\Controllers\HomeController@devient_membre')->name('admin.membres_inscrit_save');

Route::group(['prefix' => 'param'], function () {
    Route::get('banniere', 'App\Http\Controllers\Admin\ParametreController@index')->name('param.banniere');
    Route::get('banniere_add', 'App\Http\Controllers\Admin\ParametreController@banniere_add')->name('param.banniere_add');
    Route::get('banniere_edit/{id}', 'App\Http\Controllers\Admin\ParametreController@banniere_add')->name('param.banniere_edit');
    Route::post('banniere_save', 'App\Http\Controllers\Admin\ParametreController@banniere_save')->name('param.banniere_save');
    Route::post('banniere_update', 'App\Http\Controllers\Admin\ParametreController@banniere_save')->name('param.banniere_update');

    Route::get('partenaire', 'App\Http\Controllers\Admin\ParametreController@partenaire')->name('param.partenaire');
    Route::get('partenaire_add', 'App\Http\Controllers\Admin\ParametreController@partenaire_add')->name('param.partenaire_add');
    Route::get('partenaire_edit/{id}', 'App\Http\Controllers\Admin\ParametreController@partenaire_add')->name('param.partenaire_edit');
    Route::post('partenaire_save', 'App\Http\Controllers\Admin\ParametreController@partenaire_save')->name('param.partenaire_save');
    Route::post('partenaire_update', 'App\Http\Controllers\Admin\ParametreController@partenaire_save')->name('param.partenaire_update');

    Route::get('ville_commune', 'App\Http\Controllers\Admin\ParametreController@ville_commune')->name('param.ville_commune');
    Route::get('ville_commune_add', 'App\Http\Controllers\Admin\ParametreController@ville_commune_add')->name('param.ville_commune_add');
    Route::get('ville_commune_edit/{id}', 'App\Http\Controllers\Admin\ParametreController@ville_commune_add')->name('param.ville_commune_edit');
    Route::post('ville_commune_save', 'App\Http\Controllers\Admin\ParametreController@ville_commune_save')->name('param.ville_commune_save');
    Route::post('ville_commune_update', 'App\Http\Controllers\Admin\ParametreController@ville_commune_save')->name('param.ville_commune_update');

    Route::post('delete_partenaire', 'App\Http\Controllers\Admin\ParametreController@delete_partenaire')->name('param.delete_partenaire');
    Route::post('delete_banniere', 'App\Http\Controllers\Admin\ParametreController@delete_banniere')->name('param.delete_banniere');

    Route::get('tags', 'App\Http\Controllers\Admin\ParametreController@tags')->name('param.tags');
    Route::get('tags_add', 'App\Http\Controllers\Admin\ParametreController@tags_add')->name('param.tags_add');
    Route::get('tags_edit/{id}', 'App\Http\Controllers\Admin\ParametreController@tags_add')->name('param.tags_edit');
    Route::post('tags_save', 'App\Http\Controllers\Admin\ParametreController@tags_save')->name('param.tags_save');
    Route::post('tags_update', 'App\Http\Controllers\Admin\ParametreController@tags_save')->name('param.tags_update');
    Route::post('delete_tag', 'App\Http\Controllers\Admin\ParametreController@delete_tag')->name('param.delete_tag');

});

Route::group(['prefix' => 'media'], function () {
    Route::get('mediatheque', 'App\Http\Controllers\Admin\MediathequeController@index')->name('admin.mediatheque');
    Route::get('mediatheque_add', 'App\Http\Controllers\Admin\MediathequeController@mediatheque_add')->name('admin.mediatheque_add');
    Route::get('mediatheque_edit/{id}', 'App\Http\Controllers\Admin\MediathequeController@mediatheque_add')->name('admin.mediatheque_edit');
    Route::post('mediatheque_save', 'App\Http\Controllers\Admin\MediathequeController@mediatheque_save')->name('admin.mediatheque_save');
    Route::post('mediatheque_update', 'App\Http\Controllers\Admin\MediathequeController@mediatheque_save')->name('admin.mediatheque_update');
    Route::post('mediatheque_delete', 'App\Http\Controllers\Admin\MediathequeController@mediatheque_delete')->name('admin.mediatheque_delete');
    Route::post('piece_delete', 'App\Http\Controllers\Admin\MediathequeController@piece_delete')->name('admin.piece_delete');
});
